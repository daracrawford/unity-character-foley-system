﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedRaycast : MonoBehaviour
{
    public RaycastHit hit_isPlayerGrounded;

    public bool inTheAir = false;


    public void PlayerGroundedCheck()
    {
        Vector3 downward = transform.TransformDirection(Vector3.down);

        if (Physics.Raycast(transform.position, downward, out hit_isPlayerGrounded))
        {
            if (hit_isPlayerGrounded.distance > 0.1)
            {
                //Debug.Log("Player is in the air");
                inTheAir = true;
            }
        }
        else
        {
            inTheAir = false;
            Debug.Log("Player is not in the air");

        }
    }
    


}
