﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "Add New Surface Audio Clips", fileName = "Surface Name", order = 1)]
public class SurfaceAudioClips : ScriptableObject
{
    [Header("Add First Set Of Audio Clips")]
    [Tooltip("At least one audio clip is required for Clips One")]
    [SerializeField] public List<AudioClip> clipsOne;
    
    [Header("Optional Concatenation.  Add 2nd Set Of Clips")]
    [Tooltip("If a second set of clips are added, a random audio clip from one and two will play in sequence")]
    [SerializeField] public List<AudioClip> clipsTwo;
}
