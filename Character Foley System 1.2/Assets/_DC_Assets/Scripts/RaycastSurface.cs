﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Experimental.PlayerLoop;

public class RaycastSurface : MonoBehaviour
{
    [Header("Script References")]
    // Script References.
    [SerializeField] FoleyEventTrigger foleyTriggerScript;
    [SerializeField] FootstepSystem footstepScript;
    [SerializeField] private GroundedRaycast _raycastLeftFootCheck;
    [SerializeField] private GroundedRaycast _raycastRightFootCheck;



    public bool debugMode;
    private bool isPlayerColliding;

    // Tracks Player Collision Status
    public static bool is_CollidingWithGameObject;
    public static bool is_CollidingWithTerrain;
    public static bool is_PlayerGrounded;
    public static bool playerInAir;
    public static bool playerLanded;

    private Vector3 lookAhead;


    public AudioMixerGroup FoleyMaster;
    private float jumpVolume;
    private float saveJumpVolume;

    [Header("Surface Check Settings")]
    public bool automateSurfaceDetection;

    [Tooltip("Only active if surface detection is switched on")]
    [Range(0,1)]
    public double surfaceCheckSpeed;



    // Raycast Variables.
    public RaycastHit hit;
    public RaycastHit hit_groundedCheck;
    public static float theDistance;
    private Vector3 downward;

    void Awake()
    {
        // Starts Surface Detetcion.
        StartSurfaceDetetcion();
    }

    // Start is called before the first frame update.
    void Start()
    {
        // Save Original Volume
        FoleyMaster.audioMixer.GetFloat("volFoleyMaster", out saveJumpVolume);
    }

    public void SurfaceDetection()
    {
        if (automateSurfaceDetection)
        {
            Vector3 downward = transform.TransformDirection(Vector3.down) * 2;
            Debug.DrawRay(transform.position, downward, Color.red);

            if (Physics.Raycast(transform.position, downward, out hit))
            {
                // Updates surface distance.
                theDistance = hit.distance;
                if (debugMode) { Debug.Log(theDistance); }


                if (hit.collider.gameObject.CompareTag("Surface_Terrain"))
                {
                    // Calls Terrain Splat Texture Check
                    foleyTriggerScript.TerrainSurfaceSwitch();

                    is_CollidingWithGameObject = false;
                    is_CollidingWithTerrain = true;
                }
                else
                {
                    // Switches Audio Depending On GameObject Tag
                    is_CollidingWithGameObject = true;
                    is_CollidingWithTerrain = false;

                    SurfaceGameObjectSwitch();
                }
            }

            // Detects if player has jumped.
            LandDetection();

        }
    }

    public void LandDetection()
    {
      
            
        
    }

    public void StartSurfaceDetetcion()
    {
        // Starts Surface Detetction.
        InvokeRepeating("SurfaceDetection", 0.1f, foleyTriggerScript.surfaceCheckInterval);
    }


    public void StopSurfaceDetetcion()
    {
        // Stops Surface Detetction.
        CancelInvoke("SurfaceObjectDetection");
    }

    void SurfaceGameObjectSwitch()
    {
        if (hit.collider.gameObject.CompareTag("Surface_Concrete"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.ConcreteSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on concrete");
            }

        }

        if (hit.collider.gameObject.CompareTag("Surface_Carpet"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.CarpetSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on carpet");
            }
        }


        if (hit.collider.gameObject.CompareTag("Surface_Wood"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.WoodSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on wood");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Grass"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.GrassSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on grass");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Gravel_Bright"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.GravelBrightSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on gravel");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Gravel_Dark"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.GravelDarkSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on gravel");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Sand_Shallow"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.SandShallowSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on shallow sand");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Sand_Deep"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.SandDeepSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on deep sand");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Sand_Wet"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.SandWetSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on wet sand");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Ice"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.IceSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on ice");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Water"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.WaterSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on water");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Rock"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.RockSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on rock");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Tile_Bright"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.TileBrightSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on bright tile");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Tile_Dark"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.TileDarkSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on dark tile");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Custom_1"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.Custom1Steps();

            if (debugMode)
            {
                Debug.Log("Player walking on custom 1");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Custom_2"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.Custom2Steps();

            if (debugMode)
            {
                Debug.Log("Player walking on custom 2");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Custom_3"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.Custom3Steps();

            if (debugMode)
            {
                Debug.Log("Player walking on custom 3");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Custom_4"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.Custom4Steps();

            if (debugMode)
            {
                Debug.Log("Player walking on custom 4");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Dirt"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.DirtSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on dirt");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Snow_Deep"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.SnowDeepSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on snow deep");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Snow_Shallow"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.SnowShallowSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on snow shallow");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Metal_Bright"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.MetalBrightSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on metal bright");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Metal_Dark"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.MetalDarkSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on metal Dark");
            }
        }

        if (hit.collider.gameObject.CompareTag("Surface_Metal_Soft"))
        {
            // Player collidering is true.
            isPlayerColliding = true;

            // Debug Mode
            if (debugMode)
            {
                Debug.Log("Player colliding is true");
            }

            footstepScript.MetalSoftSteps();

            if (debugMode)
            {
                Debug.Log("Player walking on metal soft");
            }
        }
    }

}
