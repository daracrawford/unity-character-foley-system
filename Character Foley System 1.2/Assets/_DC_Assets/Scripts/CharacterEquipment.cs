﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(menuName = "Add New Equipment Type", fileName = "Equipment Material", order = 1)]
public class CharacterEquipment : ScriptableObject
{
    [Header("Add Audio Clips")]
    [SerializeField] public List<AudioClip> soundClips;
}

