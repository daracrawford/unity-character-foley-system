﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using JetBrains.Annotations;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.Audio;
using UnityEngineInternal;

public class FoleyEventTrigger : MonoBehaviour
{
    // For troubleshooting 
    public bool debugMode = false;


    [Header("Required Objects")]
    // Script References
    [SerializeField] FootstepSystem footstepScript;
    [SerializeField] CharacterEquipped equippedScript;
    [SerializeField] ThirdPersonCharacter characterController;


 // Ref Player character
    public Rigidbody character;

    // Raycast Variables
    private RaycastHit hit;
    private float theDistance;
    private Vector3 downward;



    // Animation Variables
    private Animator anim;              // Get floats from animator curves.
    private float activeKeyframeLeft;   // Animation key frame float.
    private float lastKeyframeLeft;     // Animation key frame float.
    private float activeKeyframeRight;  // Animation key frame float.
    private float lastKeyframeRight;    // Animation key frame float.

    [Range(0f, 60f)]
    public float characterDryTime;      // How long it takes for character to dry




    //TERRAIN VARIABLES
    private int surfaceIndex;
    private Terrain terrain;
    private TerrainData terrainData;
    private Vector3 terrainPos;
    
    // Surface Check Time Interval.
    [Header("How often should the surface be checked?")]
    [Range(0f, 1f)] public float surfaceCheckInterval;

   // Checks if player is colliding with surface 
    private bool isPlayerColliding;
    private bool is_TerrainCheck = false;
    private bool is_GameObjectCheck = false;


    // Surface Options
    public enum SurfaceType
    {
        none,
        carpet,
        concrete,
        custom_1,
        custom_2,
        custom_3,
        custom_4,
        dirt,
        grass,
        gravelBright,
        gravelDark,
        ice,
        metalBright,
        metalDark,
        metalSoft,
        mud,
        rock,
        sandDryDeep,
        sandDryShallow,
        sandWet,
        snowDeep,
        snowShallow,
        tileBright,
        tileDark,
        water,
        wood,
    };

    [Header("Terrain Texture Sync")]
    // To Sync With Each Active Terrain Texture.
    public SurfaceType terrainTexture0;
    public SurfaceType terrainTexture1;
    public SurfaceType terrainTexture2;
    public SurfaceType terrainTexture3;
    public SurfaceType terrainTexture4;
    public SurfaceType terrainTexture5;
    public SurfaceType terrainTexture6;
    public SurfaceType terrainTexture7;
    public SurfaceType terrainTexture8;
    public SurfaceType terrainTexture9;
    public SurfaceType terrainTexture10;
    public SurfaceType terrainTexture11;
    public SurfaceType terrainTexture12;
    public SurfaceType terrainTexture13;
    public SurfaceType terrainTexture14;
    public SurfaceType terrainTexture15;
    public SurfaceType terrainTexture16;
    public SurfaceType terrainTexture17;
    public SurfaceType terrainTexture18;
    public SurfaceType terrainTexture19;
    public SurfaceType terrainTexture20;



   

    // Use this for initialization
    void Start()
    {
        // Sets terrain variables.
        terrain = Terrain.activeTerrain;
        terrainData = terrain.terrainData;
        terrainPos = terrain.transform.position;

        // Calls Surface Switch.
        //InvokeRepeating("SurfaceDetection", 0.1f, surfaceCheckInterval);
    }






    // Texture Float
    private float[] GetTextureMix(Vector3 WorldPos)
    {
        // returns an array containing the relative mix of textures
        // on the main terrain at this world position.

        // The number of values in the array will equal the number
        // of textures added to the terrain.

        // calculate which splat map cell the worldPos falls within (ignoring y)
        int mapX = (int)(((WorldPos.x - terrainPos.x) / terrainData.size.x) * terrainData.alphamapWidth);
        int mapZ = (int)(((WorldPos.z - terrainPos.z) / terrainData.size.z) * terrainData.alphamapHeight);

        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = terrainData.GetAlphamaps(mapX, mapZ, 1, 1);

        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];

        for (int n = 0; n < cellMix.Length; n++)
        {
            cellMix[n] = splatmapData[0, 0, n];
        }

        return cellMix;
    }

    // Returns texture index based on world position
    private int GetMainTexture(Vector3 WorldPos)
    {
        // returns the zero-based index of the most dominant texture
        // on the main terrain at this world position.
        float[] mix = GetTextureMix(WorldPos);

        float maxMix = 0;
        int maxIndex = 0;

        // loop through each mix value and find the maximum
        for (int n = 0; n < mix.Length; n++)
        {
            if (mix[n] > maxMix)
            {
                maxIndex = n;
                maxMix = mix[n];
            }
        }
        return maxIndex;
    }




  
    // Triggers footstep event
    public void FootstepLeft(string movementType)
    {
       if (movementType == "Walk")
        {
            if (footstepScript.characterSpeed > 0.1f && footstepScript.characterSpeed < 1.2f)
            {
                footstepScript.Footstep();

                if (debugMode){Debug.Log("Footstep Left Walk");}
            }
        }

        if (movementType == "Run")
        {
            if (footstepScript.characterSpeed >= 1.2f)
            {
                footstepScript.Footstep();
                if (debugMode) { Debug.Log("Footstep Left Run"); }
            }
        }
    }

    // Triggers footstep event
    public void FootstepRight(string movementType)
    {
        // If movementType parameter is equal to walk.
        if (movementType == "Walk")
        {
            // If Speed is greather than 0.1 and less than 1.2.
            if (footstepScript.characterSpeed > 0.1f && footstepScript.characterSpeed < 1.2f)
            {
                footstepScript.Footstep();
                if (debugMode) { Debug.Log("Footstep Right Walk"); }

            }
        }

        // If movementType parameter is equal to run.
        if (movementType == "Run")
        {
            // If Speed is greather than 1.2.
            if (footstepScript.characterSpeed >= 1.2f)
            {
                footstepScript.Footstep();
                if (debugMode) { Debug.Log("Footstep Right Run"); }
            }
        }
    }

   
   

    #region Individual Calls

    public void ClothingLow1()
    {
        equippedScript.LowClothingSound1();
    }

    public void ClothingLow2()
    {
        equippedScript.LowClothingSound2();

    }
    public void ClothingUpper1()
    {
        equippedScript.UpperClothingSound1();

    }
    public void ClothingUpper2()
    {
        equippedScript.UpperClothingSound2();
    }
    public void AcessoryLow1()
    {
        equippedScript.LowAccessorySound1();

    }
    public void AcessoryLow2()
    {
        equippedScript.LowAccessorySound2();

    }
    public void AcessoryUpper1()
    {
        equippedScript.UpperAccessorySound1();

    }
    public void AcessoryUpper2()
    {
        equippedScript.UpperAccessorySound2();}


    #endregion
  

    public void TerrainSurfaceSwitch()
    {
        // Updates surface index depening on character position.
        surfaceIndex = GetMainTexture(transform.position);

        // Prinys current surface index.
        if (debugMode == true) { Debug.Log("The surface index is: " + surfaceIndex); }

        // Run switch based on surface Index variable.
        switch (surfaceIndex)
        {
            #region Terrain Texture Switches
            case 0:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture0)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch 0");
                    }
                }
                break;
            case 1:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture1)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 1");
                    }
                }
                break;
            case 2:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture2)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 2");
                    }
                }
                break;
            case 3:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture3)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 3");
                    }
                }
                break;
            case 4:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture4)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 4");
                    }
                }
                break;
            case 5:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture5)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 5");
                    }
                }
                break;
            case 6:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture6)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 6");
                    }
                }
                break;
            case 7:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture7)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 7");
                    }
                }
                break;
            case 8:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture8)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 8");
                    }
                }
                break;
            case 9:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture9)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 9");
                    }
                }
                break;
            case 10:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture10)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 10");
                    }
                }
                break;
            case 11:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture11)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 11");
                    }
                }
                break;
            case 12:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture12)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 12");
                    }
                }
                break;
            case 13:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture13)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 13");
                    }
                }
                break;
            case 14:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture14)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 14");
                    }
                }
                break;
            case 15:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture15)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 15");
                    }
                }
                break;
            case 16:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture16)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 16");
                    }
                }
                break;
            case 17:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture17)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 17");
                    }
                }
                break;
            case 18:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture18)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 18");
                    }
                }
                break;
            case 19:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture19)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 19");
                    }
                }
                break;
            case 20:
                {
                    // Calls Footstep Script & Triggers The Correct Footstep Switch.
                    switch (terrainTexture20)
                    {
                        case SurfaceType.none:
                            if (debugMode == true) { Debug.Log("Surface set to none."); }
                            break;
                        case SurfaceType.carpet:
                            footstepScript.CarpetSteps();
                            break;
                        case SurfaceType.concrete:
                            footstepScript.ConcreteSteps();
                            break;
                        case SurfaceType.custom_1:
                            footstepScript.Custom1Steps();
                            break;
                        case SurfaceType.custom_2:
                            footstepScript.Custom2Steps();
                            break;
                        case SurfaceType.custom_3:
                            footstepScript.Custom3Steps();
                            break;
                        case SurfaceType.custom_4:
                            footstepScript.Custom4Steps();
                            break;
                        case SurfaceType.dirt:
                            footstepScript.DirtSteps();
                            break;
                        case SurfaceType.grass:
                            footstepScript.GrassSteps();
                            break;
                        case SurfaceType.gravelBright:
                            footstepScript.GravelBrightSteps();
                            break;
                        case SurfaceType.gravelDark:
                            footstepScript.GravelDarkSteps();
                            break;
                        case SurfaceType.ice:
                            footstepScript.IceSteps();
                            break;
                        case SurfaceType.metalBright:
                            footstepScript.MetalBrightSteps();
                            break;
                        case SurfaceType.metalDark:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.metalSoft:
                            footstepScript.MetalDarkSteps();
                            break;
                        case SurfaceType.mud:
                            footstepScript.MudSteps();
                            break;
                        case SurfaceType.rock:
                            footstepScript.RockSteps();
                            break;
                        case SurfaceType.sandDryDeep:
                            footstepScript.SandDeepSteps();
                            break;
                        case SurfaceType.sandDryShallow:
                            footstepScript.SandShallowSteps();
                            break;
                        case SurfaceType.sandWet:
                            footstepScript.SandWetSteps();
                            break;
                        case SurfaceType.snowDeep:
                            footstepScript.SnowDeepSteps();
                            break;
                        case SurfaceType.snowShallow:
                            footstepScript.SnowShallowSteps();
                            break;
                        case SurfaceType.tileBright:
                            footstepScript.TileBrightSteps();
                            break;
                        case SurfaceType.tileDark:
                            footstepScript.TileDarkSteps();
                            break;
                        case SurfaceType.water:
                            footstepScript.WaterSteps();
                            break;
                        case SurfaceType.wood:
                            footstepScript.WoodSteps();
                            break;
                    }

                    if (debugMode == true)
                    {
                        Debug.Log("Terrain switch is 20");
                    }
                }
                break;

                #endregion
        }
       
    }

  
    void OnTriggerEnter(Collider surface)
    {
        // LAYERS
        if (surface.gameObject.CompareTag("Layer_Shattered_Glass"))
        {
            footstepScript.layerActive = true;
            isPlayerColliding = true;

            footstepScript.GlassShattered();
            if (debugMode)
            {
                Debug.Log("Glass Layer Triggered");
            }
        }
        if (surface.gameObject.CompareTag("Layer_Puddle"))
        {
            footstepScript.layerActive = true;
            isPlayerColliding = true;


            footstepScript.WaterPuddle();
            if (debugMode)
            {
                Debug.Log("Water Puddle Layer Triggered");
            }
        }
        if (surface.gameObject.CompareTag("Layer_Water_Movement"))
        {
            footstepScript.layerActive = true;
            isPlayerColliding = true;


            footstepScript.WaterMovement();
            if (debugMode)
            {
                Debug.Log("Water Movement Layer Triggered");
            }
        }
        if (surface.gameObject.CompareTag("Layer_Wood_Creek"))
        {
            footstepScript.layerActive = true;
            isPlayerColliding = true;


            footstepScript.WoodCreek();
            if (debugMode)
            {
                Debug.Log("Wood Creek Layer Triggered");
            }
        }
        if (surface.gameObject.CompareTag("Layer_Foliage"))
        {
            footstepScript.layerActive = true;
            isPlayerColliding = true;


            footstepScript.GrassFoliage();
            if (debugMode)
            {
                Debug.Log("Grass Foilage Layer Triggered");
            }
        }
    }

    void OnTriggerExit(Collider surface)
    {
        // Game Object collider is false.
        isPlayerColliding = false;

        // Layer Colliding is false.
        footstepScript.layerActive = false;

        // Colliding is false log.
        if (debugMode){Debug.Log("Player colliding is false");}
    }


    }
