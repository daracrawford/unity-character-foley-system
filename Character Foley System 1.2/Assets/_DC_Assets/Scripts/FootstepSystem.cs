﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using JetBrains.Annotations;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Tilemaps;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.Water;
using Random = UnityEngine.Random;

public class FootstepSystem : MonoBehaviour
{
    // DEBUGGING
    [HideInInspector] public bool debugMode = false;

    // Script Reference
    [SerializeField] ThirdPersonCharacter characterController;
    public float characterSpeed;


    // GAMEPLAY VARIABLES
    private bool isPlayerColliding = false;

    // SURFACE BOOL VARIABLES
    private bool isAllSurfacesDisabled;

    private bool isConcreteEnabled;
    private bool isRockEnabled;
    private bool isWoodEnabled;
    private bool isGlassEnabled;
    private bool isMetalBrightEnabled;
    private bool isMetalDarkEnabled;
    private bool isMetalSoftEnabled;
    private bool isTileBrightEnabled;
    private bool isTileDarkEnabled;
    private bool isIceEnabled;
    private bool isGravelBrightEnabled;
    private bool isGravelDarkEnabled;
    private bool isDirtEnabled;
    private bool isSandShallowEnabled;
    private bool isSandDeepEnabled;
    private bool isSandWetEnabled;
    private bool isGrassEnabled;
    private bool isWaterEnabled;
    private bool isMudEnabled;
    private bool isSnowShallowEnabled;
    private bool isSnowDeepEnabled;
    private bool isCarpetEnabled;
    private bool isCustom1Enabled;
    private bool isCustom2Enabled;
    private bool isCustom3Enabled;
    private bool isCustom4Enabled;



    // Is Layer Active
    private bool isLayerGrassFoilageEnabled;
    private bool isLayerGlassShatteredEnabled;
    private bool isLayerWaterPuddleEnabled;
    private bool isLayerWaterMovementEnabled;
    private bool isLayerWoodCreekEnabled;

    // Checks if the layer method should be active.
    public bool layerActive;

    private bool isLayerClipsStored;

    // Audio Source Playing
    private bool isToeClipPlaying;
    private bool isLayerClipPlaying;


    // Character Wet
    public bool isCharacterWet;
    private bool isCharacterWetClipsStored = false;


    // AUDIO BUSSES
    // Surfaces
    public AudioMixerGroup carpetBus;
    public AudioMixerGroup concreteBus;
    public AudioMixerGroup rockBus;
    public AudioMixerGroup woodBus;
    public AudioMixerGroup metalBrightBus;
    public AudioMixerGroup metalDarkBus;
    public AudioMixerGroup metalSoftBus;
    public AudioMixerGroup tileBrightBus;
    public AudioMixerGroup tileDarkBus;
    public AudioMixerGroup iceBus;
    public AudioMixerGroup gravelBrightBus;
    public AudioMixerGroup gravelDarkBus;
    public AudioMixerGroup dirtBus;
    public AudioMixerGroup grassBus;
    public AudioMixerGroup sandShallowBus;
    public AudioMixerGroup sandDeepBus;
    public AudioMixerGroup sandWetBus;
    public AudioMixerGroup waterBus;
    public AudioMixerGroup mudBus;
    public AudioMixerGroup snowDeepBus;
    public AudioMixerGroup snowShallowBus;
    public AudioMixerGroup customBus1;
    public AudioMixerGroup customBus2;
    public AudioMixerGroup customBus3;
    public AudioMixerGroup customBus4;



    // Surface Layers
    public AudioMixerGroup layerGrassFoliageBus;
    public AudioMixerGroup layerGlassShatteredBus;
    public AudioMixerGroup layerWaterPuddleBus;
    public AudioMixerGroup layerWaterMovementBus;
    public AudioMixerGroup layerWoodCreekBus;
    public AudioMixerGroup layerCharacterWetFeetBus;







    #region "Scriptable Game Object References"

    // REFERENCE TO SCRIPTABLE OBJECTS THAT CONTAIN AUDIO FILES
    // HARD SURFACES
    [Header("Hard Surface Audio Clip References")] [SerializeField]
    private SurfaceAudioClips concrete_SGO; // Concrete

    [SerializeField] private SurfaceAudioClips rock_SGO; // Rock
    [SerializeField] private SurfaceAudioClips wood_SGO; // Wood
    [SerializeField] private SurfaceAudioClips metalBright_SGO; // Metal bright
    [SerializeField] private SurfaceAudioClips metalDark_SGO; // Metal dark
    [SerializeField] private SurfaceAudioClips metalSoft_SGO; // Metal soft


    [SerializeField] private SurfaceAudioClips tileBright_SGO; // Tiled
    [SerializeField] private SurfaceAudioClips tileDark_SGO; // Tiled
    [SerializeField] private SurfaceAudioClips ice_SGO; // Ice
    [SerializeField] private SurfaceAudioClips gravelBright_SGO; // Gravel Bright
    [SerializeField] private SurfaceAudioClips gravelDark_SGO; // Gravel Dark
    [SerializeField] private SurfaceAudioClips dirt_SGO; // Dirt

    // SOFT SURFACES
    [Header("Soft Surface Audio Clip References")] [SerializeField]
    private SurfaceAudioClips grass_SGO; // Grass

    [SerializeField] private SurfaceAudioClips sandDryShallow_SGO; // Sand dry shallow
    [SerializeField] private SurfaceAudioClips sandDryDeep_SGO; // Sand dry deep
    [SerializeField] private SurfaceAudioClips sandWet_SGO; // Sand wet
    [SerializeField] private SurfaceAudioClips water_SGO; // Water
    [SerializeField] private SurfaceAudioClips mud_SGO; // Mud
    [SerializeField] private SurfaceAudioClips snowDeep_SGO; // Snow deep
    [SerializeField] private SurfaceAudioClips snowShallow_SGO; // Snow shallow
    [SerializeField] private SurfaceAudioClips carpet_SGO; // Carpet

    [Header("Custom Slots")]
    [SerializeField] private SurfaceAudioClips custom1_SGO; // Custom slot 1
    [SerializeField] private SurfaceAudioClips custom2_SGO; // Custom slot 2
    [SerializeField] private SurfaceAudioClips custom3_SGO; // Custom slot 3
    [SerializeField] private SurfaceAudioClips custom4_SGO; // Custom Slot 4
    


    // SURFACE LAYERS - Blends with the surfaces above
    [Header("Surface Layers Audio Clip References")]
    [SerializeField] private SurfaceAudioClips layer_grassFoliage_SGO;      // Long grass
    [SerializeField] private SurfaceAudioClips layer_glassShattered_SGO;    // Shattered glass
    [SerializeField] private SurfaceAudioClips layer_waterPuddle_SGO;       // Water puddle
    [SerializeField] private SurfaceAudioClips layer_waterMovement_SGO;     // Water 
    [SerializeField] private SurfaceAudioClips layer_woodCreek_SGO;         // Wood creek


    #endregion



    #region "Audio Sample Lists"

    // AUDIO SAMPLE LISTS 
    // HARD SURFACES
    private List<AudioClip> concreteHeel;                               // Concrete
    private List<AudioClip> concreteToe;
    private List<AudioClip> woodHeel;                                   // Wood
    private List<AudioClip> woodToe;
    private List<AudioClip> rockHeel;                                   // Rock
    private List<AudioClip> rockToe;
    private List<AudioClip> metalBrightHeel;                                  // Metal
    private List<AudioClip> metalBrightToe;
    private List<AudioClip> metalDarkHeel;                                  // Metal
    private List<AudioClip> metalDarkToe;
    private List<AudioClip> metalSoftHeel;                                  // Metal
    private List<AudioClip> metalSoftToe;
    private List<AudioClip> tileBrightHeel;                                   // Tiled
    private List<AudioClip> tileBrightToe;
    private List<AudioClip> tileDarkHeel;                                   // Tiled
    private List<AudioClip> tileDarkToe;
    private List<AudioClip> glassHeel;                                  // Glass
    private List<AudioClip> glassToe;
    private List<AudioClip> iceHeel;                                    // Ice
    private List<AudioClip> iceToe;

    // LOOSE SURFACES
    private List<AudioClip> dirtHeel;                                   // Dirt
    private List<AudioClip> dirtToe;
    private List<AudioClip> gravelBrightHeel;                           // Gravel
    private List<AudioClip> gravelBrightToe;
    private List<AudioClip> gravelDarkHeel;                           // Gravel
    private List<AudioClip> gravelDarkToe;
    // SOFT SURFACES
    private List<AudioClip> grassHeel;                                  // Grass
    private List<AudioClip> grassToe;                       
    private List<AudioClip> sandShallowHeel;                                   // Sand
    private List<AudioClip> sandShallowToe;
    private List<AudioClip> sandDeepHeel;                                   // Sand
    private List<AudioClip> sandDeepToe;
    private List<AudioClip> sandWetHeel;                                   // Sand
    private List<AudioClip> sandWetToe;
    private List<AudioClip> waterHeel;                                  // Water
    private List<AudioClip> waterToe;
    private List<AudioClip> carpetHeel;                                 // Carpet
    private List<AudioClip> carpetToe;
    private List<AudioClip> snowShallowHeel;                                   // Snow
    private List<AudioClip> snowShallowToe;
    private List<AudioClip> snowDeepHeel;                                   // Snow
    private List<AudioClip> snowDeepToe;
    private List<AudioClip> mudHeel;                                    // Mud
    private List<AudioClip> mudToe;
    private List<AudioClip> custom1Heel;                                    // Custom slots
    private List<AudioClip> custom1Toe;
    private List<AudioClip> custom2Heel;                                    // Custom slots
    private List<AudioClip> custom2Toe;
    private List<AudioClip> custom3Heel;                                    // Custom slots
    private List<AudioClip> custom3Toe;
    private List<AudioClip> custom4Heel;                                    // Custom slots
    private List<AudioClip> custom4Toe;

    // SURFACE LAYERS
    private List<AudioClip> grassFoilage;                                  // Long Grass
    private List<AudioClip> glassShattered;                             // Glass Shattered
    private List<AudioClip> waterPuddle;                                      // Scrap
    private List<AudioClip> water;                                      // Water
    private List<AudioClip> woodCreek;                                      // Water

    private List<AudioClip> characterWet;
    private List<AudioClip> activeCharacterWetClips;




    #endregion



    public AudioSource[] soundSources;                      // An array to store audio sources.
   [SerializeField] private List<AudioClip> heelClips;     // First list of audio clips.
   [SerializeField] private List<AudioClip> toeClips;      // Second list of audio clips.

    private List<AudioClip> activeLayerClips;               // Active Layer Clips.


    private List<AudioClip> storeHeelClips;                 // Stores a copy of audio clips 1.
    private List<AudioClip> storeToeClips;                  // Store a copy of audio clips 2.

    private List<AudioClip> storeLayerClips;                // Store layer clips.
    private List<AudioClip> storeCharacterWetClips;

    private List<AudioClip> refreshHeelClips;               // Refresh heel clips.
    private List<AudioClip> refreshToeClips;                // Refresh toe clips.

    private List<AudioClip> refreshLayerClips;              // Refresh layer clips. 


    [Header("Set Delay Between Heel & Toe")]
    [HideInInspector] [Range(0, 1)] public double toeDelay = 0f;        // Add a controllable delay between clip 1 and 2.

    private double clipDuration;                                        // Length of audio clip.
    [HideInInspector] public bool isActiveClipsStored = false;          // Checks if audio clips have been stored.
    private bool surfaceChanged;                                        // Checks if the surface has changed. 


    // VOLUME VARIABLES
    [HideInInspector] public float volume;                  // Default volume set by inspector slider.
    private float saveVolume;
    [HideInInspector] public float minVolume = 0.1f;        // Min volume.
    [HideInInspector] public float maxVolume = 1f;          // Max volume.

    public float randomVolume;                              // Randomise volume.
    float newVolume;                                        // Overrides current volume.
    float addVolume;                                        // The amount of volume to add onto the default volume.
    float subVolume;                                        // The amount of volume to substract from the default volume.  

    private bool isVolSaved = false;

    // Footstep Nuisance Control Variables
    public bool footstepNuisanceControl;
    public bool targetReached;
    public float stepReductionTarget;
    public float targetSpeed;


    // PITCH VARIABLES
    [HideInInspector] public bool pitchEnable;              // Resonance random variance.


    [HideInInspector] public float pitch;                   // Default pitch set by inspector slider.
    private float savePitch;                                // Saves a copy of the users pitch.
    private float minPitch = 0.5f;                          // Min pitch.
    private float maxPitch = 2f;                            // Max pitch.   

    [HideInInspector] public float randomPitch;             // Randomise pitch.
    [HideInInspector] public float newPitch;                // Overrides current pitch.
    [HideInInspector] public float addPitch;                // Adds pitch.
    [HideInInspector] public float subPitch;                // Subtracts pitch.


    // LOW PASS FILTER VARIABLES
    public AudioLowPassFilter[] LowPassFilters;             // Low Pass Filter Reference.
    [HideInInspector] public bool lpf_Enable;                // Resonance random variance.


    [SerializeField]
    [HideInInspector] public float lpf_FreqCut;             // Low freq cut.
    private float lpf_SaveFreqCut;                          // Save original value.
    [HideInInspector] public float lpf_Min = 10;            // Min value.
    [HideInInspector] public float lpf_Max = 22000;         // Max value.

    private float lpf_NewFreqCut;                           // New freq value.
    private float lpf_Add;                                  // Add amount.
    private float lpf_Sub;                                  // Subtract amount.
    [HideInInspector] public float lpf_Random;              // Random value variance. 

    [HideInInspector] public float lpf_Resonance;           // LPF Resonance Q.
    private float lpf_NewResonance;                         // LPF New Resonance Q.
    private float lpf_ResAdd;                               // Add resonance.
    private float lpf_ResSub;                               // Subtract Resonance.
    [HideInInspector] public float lpf_ResMax = 3;          // Max Res.
    [HideInInspector] public float lpf_ResMin = 0;          // Min Res.
    [HideInInspector] public float lpf_ResRandom;           // Resonance random variance.


    // High PASS FILTER VARIABLES
    public AudioHighPassFilter[] HighPassFilters;            // High Pass Filter Reference.
    [HideInInspector] public bool hpf_Enable;                // Resonance random variance.


    [SerializeField]
    [HideInInspector] public float hpf_FreqCut;             // High freq cut.
    private float hpf_SaveFreqCut;                          // Save original value.
    [HideInInspector] public float hpf_Min = 10;            // Min value.
    [HideInInspector] public float hpf_Max = 22000;         // Max value.

    private float hpf_NewFreqCut;                           // New freq value.
    private float hpf_Add;                                  // Add amount.
    private float hpf_Sub;                                  // Subtract amount.
    [HideInInspector] public float hpf_Random;              // Random value variance. 

    [HideInInspector] public float hpf_Resonance;           // HPF Resonance Q.
    private float hpf_NewResonance;                         // HPF New Resonance Q.
    private float hpf_ResAdd;                               // Add resonance.
    private float hpf_ResSub;                               // Subtract Resonance.
    [HideInInspector] public float hpf_ResMax = 3;          // Max Res.
    [HideInInspector] public float hpf_ResMin = 0;          // Min Res.
    [HideInInspector] public float hpf_ResRandom;           // Resonance random variance.


    // CROUCH
    // Mix Snapshots
    public AudioMixerSnapshot normalMix;
    public AudioMixerSnapshot crouchMix;

    private bool crouchEnabled;

    // Automates Heel Toe Delay Based On Character Speed
    public bool heelToeAutomation;
    public float heelToeIntervalTime;


    void OnAwake()
    {
        // Stores user volume before game start.
        saveVolume = volume;

        // On awake set default as steps incase nothing is auto selected.
       // ConcreteSteps();
    }

    void Start()
    {
        // Check character Speed
        InvokeRepeating("CharacterSpeed", 0.1f, 0.5f);

        // Print character speed
        if (debugMode){ Debug.Log("Character Speed: " + characterSpeed);}
    }


    

    public void Footstep()
    {
        // If the active audio clips have been stored, run this method.
        if (isActiveClipsStored == true)
        {
            // Refresh active audio clips when list empty.
            RefreshAudioList();

            

            // A variable for randomly choosing an audio clip from the list.
            int indexClip1 = Random.Range(0, heelClips.Count);
            int indexClip2 = Random.Range(0, toeClips.Count);

            if (debugMode) { Debug.Log("Randomly selected " + indexClip1 + " from the first set of audio clips"); }
            if (debugMode) { Debug.Log("Randomly selected " + indexClip2 + " from the second set of audio clips"); }


            // Prepares the audio source with the next clip.
            soundSources[0].clip = heelClips[indexClip1];

            // The Toe Audio Source has a longer decay and release/  This will check if it has finished playing.
            if (soundSources[1].isPlaying)
            {
                // Toe clip playing is equal to true.
                isToeClipPlaying = true;
                // Reroute toe clip to audio source 2.
                soundSources[2].clip = toeClips[indexClip2];

                // Debug to ensure function is working.
                if (debugMode) { Debug.Log("Toe Clip Playing: " + isToeClipPlaying); }
            }
            else
            {
                // Toe clip playing is equal to false.
                isToeClipPlaying = false;
                // Toe clip routed to audio source 1.
                soundSources[1].clip = toeClips[indexClip2];

                // Debug to ensure function is working.
                if (debugMode) { Debug.Log("Toe Clip Playing: " + isToeClipPlaying); }
            }


            // Clip length equals the length of clip 1.
            clipDuration = (double)soundSources[0].clip.samples /                                    // Clip 1 divided by 
                           soundSources[0].clip.frequency;                                           // Clip 1 sample rate divided by clip 1 frequency.  Formula calculates the track length.

            // Pitch modulation method.
            if (pitchEnable)
            { PitchModulation(); }

            // Low Filter Modulation.
            if (lpf_Enable)
            { LowPassFilterMod(); }

            // High Pass Filter Modulation.
            if (hpf_Enable)
            { HighPassFilterMod(); }

            // Footstep Nuisance Control.
            if (footstepNuisanceControl)
            { FootstepNuisanceControl(); }

            // Volume modulation method. 
            VolumeModulation();

            // If Heel Toe Automation is true.  Call Heel toe method.
            if (heelToeAutomation)
            { HeelToeAutomation(); }

            // Else if heel toe automation is false.
            if (!heelToeAutomation)
            {   // Default audio source play settings.
                // If toe clip is playing, schedule audio source 2.
                if (isToeClipPlaying)
                {
                    // Adds a small delay using DSP time so that both audio clips sync in time. 
                    soundSources[0].PlayScheduled(AudioSettings.dspTime + 0.1);                              // Play after small dsp delay (unnoticable delay).
                    soundSources[2].PlayScheduled(AudioSettings.dspTime + 0.1 +                              // Same delay using dsp time, both instance 1 and 2 are in sync with dsp time.
                                                  clipDuration                                               // DSP delay (Syncs with clip 1) Add clip 1 length, (Plays clip 2 after clip 1). 
                                                  + toeDelay);                                               // And add concatenation delay.  
                    if (debugMode) { Debug.Log("Audio source busy, rerouted audio for layering effect"); }
                }
                if (!isToeClipPlaying)
                {
                    // Adds a small delay using DSP time so that both audio clips sync in time. 
                    soundSources[0].PlayScheduled(AudioSettings.dspTime + 0.1);                              // Play after small dsp delay (unnoticable delay).
                    soundSources[1].PlayScheduled(AudioSettings.dspTime + 0.1 +                              // Same delay using dsp time, both instance 1 and 2 are in sync with dsp time.
                                                  clipDuration                                               // DSP delay (Syncs with clip 1) Add clip 1 length, (Plays clip 2 after clip 1). 
                                                  + toeDelay);                                               // And add concatenation delay.  
                    if (debugMode) { Debug.Log("Audio audio source playing"); }
                }
            }

            // Removes the current audio clip from the list.
            heelClips.RemoveAt(indexClip1);
            toeClips.RemoveAt(indexClip2);

            if (debugMode) { Debug.Log("Removed audio clip, there are " + heelClips.Count + " clips remaining in clip set 1"); }
            if (debugMode) { Debug.Log("Removed audio clip, there are " + toeClips.Count + " clips remaining in clip set 2"); }
        }
       
        // Plays if character is wet
        if (isCharacterWet)
        {
            CharacterWetFeet();
            Debug.Log("Character Wet Steps Are Now Active");
        }

        // Plays layer if active.
        if (layerActive)
        {
            PlayLayer();
            if (debugMode) { Debug.Log("Layer Active"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Layer not active"); }

            return;
        }
        
    }

   

    public void CharacterWalking()
    {
        #region Toe Delay
        // Character Heel Toe Speed.
        // If character speed greater than 0.1 and less than 5. 
        if (characterSpeed > 0.1 && characterSpeed < 5)
        {
            // Set toe delay.
            toeDelay = 0.4;

            // New volume is current volume divide by 2
            newVolume = volume / 3;

            // Apply volume.
            foreach (var audioSource in soundSources)
            {
                audioSource.volume = newVolume;
            }
        }

        if (characterSpeed >= 5 && characterSpeed < 8)
        {
            // Set toe delay.
            toeDelay = 0.2;

            // New volume is current volume divide by 2
            newVolume = volume / 2;

            // Apply volume.
            foreach (var audioSource in soundSources)
            {
                audioSource.volume = newVolume;
            }
        }

        if (characterSpeed >= 8 && characterSpeed < 1.2)
        {
            // Set toe delay.
            toeDelay = 0;

            // New volume is standard volume.
            newVolume = volume;

            // Apply volume.
            foreach (var audioSource in soundSources)
            {
                audioSource.volume = newVolume;
            }
        }


        #endregion // Toe Delay and Volume settings


        if (isToeClipPlaying)
        {
            // Toe Delays

            // Adds a small delay using DSP time so that both audio clips sync in time. 
            soundSources[0].PlayScheduled(AudioSettings.dspTime + 0.1);                              // Play after small dsp delay (unnoticable delay).
            soundSources[2].PlayScheduled(AudioSettings.dspTime + 0.1 +                             // Same delay using dsp time, both instance 1 and 2 are in sync with dsp time.
                                          clipDuration                                              // DSP delay (Syncs with clip 1) Add clip 1 length, (Plays clip 2 after clip 1). 
                                          + toeDelay);                                              // And add concatenation delay.
        }
        else
        {
            // Adds a small delay using DSP time so that both audio clips sync in time. 
            soundSources[0].PlayScheduled(AudioSettings.dspTime + 0.1);                              // Play after small dsp delay (unnoticable delay).
            soundSources[1].PlayScheduled(AudioSettings.dspTime + 0.1 +                             // Same delay using dsp time, both instance 1 and 2 are in sync with dsp time.
                                          clipDuration                                              // DSP delay (Syncs with clip 1) Add clip 1 length, (Plays clip 2 after clip 1). 
                                          + toeDelay);                                              // And add concatenation delay.
        }
    }
    

    public void CharacterRunning()
    {
        // Ensures the correct audio source is playing
        if (isToeClipPlaying)
        {
            // Plays clips at the same time.
            soundSources[0].Play();     // Play clip.
            soundSources[2].Play();     // Play clip. 
        }
        // Ensures the correct audio source is playing
        else
        {
            // Plays clips at the same time.
            soundSources[0].Play();     // Play clip.
            soundSources[1].Play();     // Play clip. 
        }
      
    }

    public void CharacterLand()
    {
        Footstep();
        CharacterRunning();
    }
  

    // Automates Heel Toe Movement Based On Character Speed
    public void HeelToeAutomation()
    {
        if (characterSpeed > 0.1f && characterSpeed < 1.2f)
        {
            CharacterWalking();
        }
        else
        {
            CharacterRunning();
        }
        



        // If character speed equal to zero.  Not moving.
        if (characterSpeed == 0)
        {
            // Picks up any bugs in the character speed.
            // And prevents false triggers.
            return;
        }

        // Player Walking Slow.
        // If character speed greater than 0 and less than or equal to 1. 
        if (characterSpeed > 0.1 && characterSpeed <= 0.5)
        {
            // Set toe delay.
            toeDelay = 0.3;

            // New volume is current volume divide by 2
            newVolume = volume / 2;

            // Apply volume.
            foreach (var audioSource in soundSources)
            {
                audioSource.volume = newVolume;
            }

            // Call character walking method.
            CharacterWalking();
        }
        // If character speed greater than 0 and less than or equal to 1. 
        if (characterSpeed > 0.5 && characterSpeed <= 1)
        {
            // Set toe delay.
            toeDelay = 0.2;

            // New volume is current volume divide by 2
            newVolume = volume / 2 + 0.1f;

            // Apply volume.
            foreach (var audioSource in soundSources)
            {
                audioSource.volume = newVolume;
            }

            // Call character walking method.
            CharacterWalking();
        }
        // Player Walking Normal to Fast.
        // If character speed greater than 1 and equal to 3
        if (characterSpeed >= 1f && characterSpeed < 3)
        {
            // Set toe delay.  Instant sequence of heel toe.
            toeDelay = 0;

            // New volume is current volume divide by 2
            newVolume = saveVolume;

            // Apply volume.
            foreach (var audioSource in soundSources)
            {
                audioSource.volume = newVolume;
            }

            // Call character walking method.
            CharacterWalking();
        }

        // Player Running.
        // If character speed greater than 3, cal running method
        if (characterSpeed > 3)
        {
            // New volume is current volume divide by 2
            newVolume = volume + 0.2f;

            // Apply volume.
            foreach (var audioSource in soundSources)
            {
                audioSource.volume = newVolume;
            }

            // Call character running method.  Plays heel toe at the same time.
            CharacterRunning();
        }
    }
    
   
    public void CharacterSpeed()
    {
        characterSpeed = characterController.GetComponent<Rigidbody>().velocity.magnitude;
        if (debugMode) { Debug.Log(("Character Speed is: " + characterSpeed)); }
    }

    public void VolumeModulation()
    {
        // For each audiosource in audiosources, do the following.
        foreach (AudioSource soundSource in soundSources)
        {
            // Restore base value.
            newVolume = volume;
            if (debugMode) { Debug.Log("Volume = " + newVolume); }

            // Add volume.
            addVolume = newVolume + randomVolume;
            if (debugMode) { Debug.Log("Add Volume: = " + addVolume); }

            // Decrease volume.
            subVolume = newVolume - randomVolume;
            if (debugMode) { Debug.Log("Decrease Volume: = " + subVolume); }

            // New volume equals a random volume between volume decrease and increase.
            newVolume = Random.Range(subVolume, addVolume);
            if (debugMode) { Debug.Log("Final Volume: = " + newVolume); }

            // Sound instance equals new volume.  Volume clamped between min and max.  
            soundSource.volume = Mathf.Clamp(newVolume, minVolume, maxVolume);
        }
    }

    
    

    public void PitchModulation()
    {
        foreach (AudioSource soundSource in soundSources)
        {
            // Restore base value.
            newPitch = pitch;

            // Add pitch.
            addPitch = newPitch + randomPitch;

            // Subtract pitch.
            subPitch = newPitch - randomPitch;

            // Randomise Pitch
            newPitch = Random.Range(subPitch, addPitch);

            // Clamp value
            soundSource.pitch = newPitch;

            Mathf.Clamp(soundSource.pitch, minPitch, maxPitch);
            

            if (debugMode) { Debug.Log("Pitch Soundsouce: " + soundSource.pitch); }
        }
    }
    
    
    public void LowPassFilterMod()
    {
        foreach (var LPF in LowPassFilters)
        {
            // PART ONE - CUT Off Calculation

            // Resets to original value.
            lpf_NewFreqCut = lpf_FreqCut;

            // Add / Subtract from the original LPF Value.
            lpf_Add = lpf_NewFreqCut + lpf_Random;
            lpf_Sub = lpf_NewFreqCut - lpf_Random;

            // Randomise LPF Value.
            lpf_NewFreqCut = Random.Range(lpf_Sub, lpf_Add);

            // Clamps value to stay within boundaries.
            Mathf.Clamp(lpf_NewFreqCut, lpf_Min, lpf_Max);

            // Sets the new LPF calculation.
            LPF.cutoffFrequency = lpf_NewFreqCut;
            
            Debug.Log("Low Freq: " + LPF.cutoffFrequency);


            // PART TWO - Resets Res Calculation.

            // Resets to original value.
            lpf_NewResonance = lpf_Resonance;
            
            // Add / Subtract from the original Res Value.
            lpf_ResAdd = lpf_NewResonance + lpf_ResRandom;
            lpf_ResSub = lpf_NewResonance - lpf_ResRandom;

            // Randomise Res Value.
            lpf_NewResonance = Random.Range(lpf_ResSub, lpf_ResAdd);

            // Clamps value to stay within boundaries.
            Mathf.Clamp(lpf_NewResonance, lpf_ResMin, lpf_ResMax);

            // Sets the new LPF calculation.
            LPF.lowpassResonanceQ = lpf_NewResonance;

            Debug.Log("Low Res Q: " + LPF.lowpassResonanceQ);
        }
    }

    public void HighPassFilterMod()
    {
        foreach (var HPF in HighPassFilters)
        {
            // PART ONE - CUT Off Calculation

            // Resets to original value.
            hpf_NewFreqCut = hpf_FreqCut;

            // Add / Subtract from the original LPF Value.
            hpf_Add = hpf_NewFreqCut + hpf_Random;
            hpf_Sub = hpf_NewFreqCut - hpf_Random;

            // Randomise LPF Value.
            hpf_NewFreqCut = Random.Range(hpf_Sub, hpf_Add);

            // Clamps value to stay within boundaries.
            Mathf.Clamp(hpf_NewFreqCut, hpf_Min, hpf_Max);

            // Sets the new LPF calculation.
            HPF.cutoffFrequency = hpf_NewFreqCut;

            Debug.Log("High Freq: " + HPF.cutoffFrequency);


            // PART TWO - Resonance Section

            // Resets to original value.
            hpf_NewResonance = hpf_Resonance;

            // Add / Subtract from the original Res Value.
            hpf_ResAdd = hpf_NewResonance + hpf_ResRandom;
            hpf_ResSub = hpf_NewResonance - hpf_ResRandom;

            // Randomise Res Value.
            hpf_NewResonance = Random.Range(hpf_ResSub, hpf_ResAdd);

            // Clamps value to stay within boundaries.
            Mathf.Clamp(hpf_NewResonance, hpf_ResMin, hpf_ResMax);

            // Sets the new LPF calculation.
            HPF.highpassResonanceQ = hpf_NewResonance;

            Debug.Log("High Res Q: " + HPF.highpassResonanceQ);
        }
    }
    

    public void FootstepNuisanceControl()
    {
        // Saves original volume setting.
        if (!isVolSaved)
        {
            saveVolume = volume;
            isVolSaved = true;
        }

        // If the volume is greater than the target volume
        if (volume > stepReductionTarget)
        {
            // Target reached equals false.
            targetReached = false;

            // Subtract 0.1 from volume.  
            volume = volume - targetSpeed;
        }
       
        // If volume is less than or equal to reduction target
        if (volume <= stepReductionTarget)
        {
            // If target reached equals false.
            if (!targetReached)
            {
                // Target reached equals true.
                targetReached = true;

                // Ensures the volume setting is exact. 
                volume = stepReductionTarget;
            }
            else
            {
                // Tracks if target is reached during debug mode.  
                if (debugMode)
                {
                    Debug.Log("Target already reached");
                }

                return;
            }
        }
    }

    public void RefreshAudioList()
    {
        // If the number of audio clips in the list is equal to 0, then restore audio list back to default.
        if (heelClips.Count == 0 )
        {
            refreshHeelClips = new List<AudioClip>(storeHeelClips);

            heelClips = refreshHeelClips;

            if (debugMode)
            {
                Debug.Log("Heel clips reset to the default of " + heelClips.Count + " audio clips");
            }

        }
        else
        {
            return;
        }
       
        

        if (toeClips.Count == 0)
        {
            refreshToeClips = new List<AudioClip>(storeToeClips);
            toeClips = refreshToeClips;

            if (debugMode)
            {
                Debug.Log("Toe clips reset to the default of " + toeClips.Count + " audio clips");
            }
        }
        else
        {
            return;
        }
       
    }


    public void ApplySettings()
    {
        saveVolume = volume;

        

        // Set volume setting.
        foreach (var soundSource in soundSources)
        {
            soundSource.volume = volume;
            soundSource.pitch = pitch;
        }

        if (hpf_Enable)
        {
            // Set high pass setting.
            foreach (var hpf in HighPassFilters)
            {
                hpf.cutoffFrequency = hpf_FreqCut;
                hpf.highpassResonanceQ = hpf_Resonance;
            }
        }

        if (lpf_Enable)
        {
            // Set low pass setting.
            foreach (var lpFilter in LowPassFilters)
            {
                lpFilter.cutoffFrequency = lpf_FreqCut;
                lpFilter.lowpassResonanceQ = lpf_Resonance;
            }
        }
       

        if (debugMode) { Debug.Log("Settings Applied");}
    }

    public void PresetClean() // No Modulation Preset.
    {
        // Settings to tweak.
        volume = 0.8f;
        saveVolume = volume;
        randomVolume = 0;
        pitch = 1;
        randomPitch = 0;
        hpf_FreqCut = 0;
        hpf_Random = 0;
        hpf_Resonance = 0;
        hpf_ResRandom = 0;
        lpf_FreqCut = 22000;
        lpf_Random = 0;
        lpf_Resonance = 0;
        lpf_ResRandom = 0;

        // Apply settings to all audio sources.
        foreach (var soundSource in soundSources)
        {
            soundSource.volume = volume;
            soundSource.pitch = pitch;
        }

        // Set high pass setting.
        foreach (var hpf in HighPassFilters)
        {
            hpf.cutoffFrequency = hpf_FreqCut;
            hpf.highpassResonanceQ = hpf_Resonance;
        }

        // Set low pass setting.
        foreach (var lpFilter in LowPassFilters)
        {
            lpFilter.cutoffFrequency = lpf_FreqCut;
            lpFilter.lowpassResonanceQ = lpf_Resonance;
        }

        // Enable / Disable Controls.
        pitchEnable = false;
        hpf_Enable = false;
        lpf_Enable = false;
        footstepNuisanceControl = false;
    }

    public void PresetSubtleModulation() // Subtle Modulation Preset.
    {// Settings to tweak.
        volume = 0.8f;
        saveVolume = volume;
        randomVolume = 0.1f;
        pitch = 1;
        randomPitch = 0.1f;
        hpf_FreqCut = 300;
        hpf_Random = 200;
        hpf_Resonance = 0;
        hpf_ResRandom = 0;
        lpf_FreqCut = 18000;
        lpf_Random = 4000;
        lpf_Resonance = 0;
        lpf_ResRandom = 0;

        // Apply settings to all audio sources.
        foreach (var soundSource in soundSources)
        {
            soundSource.volume = volume;
            soundSource.pitch = pitch;
        }

        // Set high pass setting.
        foreach (var hpf in HighPassFilters)
        {
            hpf.cutoffFrequency = hpf_FreqCut;
            hpf.highpassResonanceQ = hpf_Resonance;
        }

        // Set low pass setting.
        foreach (var lpFilter in LowPassFilters)
        {
            lpFilter.cutoffFrequency = lpf_FreqCut;
            lpFilter.lowpassResonanceQ = lpf_Resonance;
        }

        // Enable / Disable Controls.
        pitchEnable = true;
        hpf_Enable = true;
        lpf_Enable = true;
        footstepNuisanceControl = false;
    }

    public void PresetModerateModulation()
    {
        // Settings to tweak.
        volume = 0.8f;
        saveVolume = volume;
        randomVolume = 0.2f;
        pitch = 1;
        randomPitch = 0.2f;
        hpf_FreqCut = 300;
        hpf_Random = 500;
        hpf_Resonance = 0.1f;
        hpf_ResRandom = 0.1f;
        lpf_FreqCut = 16000;
        lpf_Random = 6000;
        lpf_Resonance = 0.1f;
        lpf_ResRandom = 0.1f;
        

        // Apply settings to all audio sources.
        foreach (var soundSource in soundSources)
        {
            soundSource.volume = volume;
            soundSource.pitch = pitch;
        }

        // Set high pass setting.
        foreach (var hpf in HighPassFilters)
        {
            hpf.cutoffFrequency = hpf_FreqCut;
            hpf.highpassResonanceQ = hpf_Resonance;
        }

        // Set low pass setting.
        foreach (var lpFilter in LowPassFilters)
        {
            lpFilter.cutoffFrequency = lpf_FreqCut;
            lpFilter.lowpassResonanceQ = lpf_Resonance;
        }

        // Enable / Disable Controls.
        pitchEnable = true;
        hpf_Enable = true;
        lpf_Enable = true;
        footstepNuisanceControl = false;
    }

    public void PresetHeavyModulation()
    {
        // Settings to tweak.
        volume = 0.7f;
        saveVolume = volume;
        randomVolume = 0.3f;
        pitch = 1.2f;
        randomPitch = 0.4f;
        hpf_FreqCut = 300;
        hpf_Random = 600;
        hpf_Resonance = 0.2f;
        hpf_ResRandom = 0.2f;
        lpf_FreqCut = 15000;
        lpf_Random = 7000;
        lpf_Resonance = 0.2f;
        lpf_ResRandom = 0.2f;


        // Apply settings to all audio sources.
        foreach (var soundSource in soundSources)
        {
            soundSource.volume = volume;
            soundSource.pitch = pitch;
        }

        // Set high pass setting.
        foreach (var hpf in HighPassFilters)
        {
            hpf.cutoffFrequency = hpf_FreqCut;
            hpf.highpassResonanceQ = hpf_Resonance;
        }

        // Set low pass setting.
        foreach (var lpFilter in LowPassFilters)
        {
            lpFilter.cutoffFrequency = lpf_FreqCut;
            lpFilter.lowpassResonanceQ = lpf_Resonance;
        }

        // Enable / Disable Controls.
        pitchEnable = true;
        hpf_Enable = true;
        lpf_Enable = true;
        footstepNuisanceControl = false;
    }

    // Restore Default Settings
    public void Reset()
    {
        heelClips = null;           // Clear audio clips.
        toeClips = null;            // Clear audio clips.

        // Default audio source settings 
        volume = 1;
        saveVolume = 1;
        pitch = 1;
        hpf_FreqCut = 0;
        hpf_Resonance = 0;
        lpf_FreqCut = 22000;
        lpf_Resonance = 0;

        // Apply settings to all audio sources.
        foreach (var soundSource in soundSources)
        {
            soundSource.volume = volume;
            soundSource.pitch = pitch;
        }
       
        // Set high pass setting.
        foreach (var hpf in HighPassFilters)
        {
            hpf.cutoffFrequency = hpf_FreqCut;
            hpf.highpassResonanceQ = hpf_Resonance;
        }

        // Set low pass setting.
        foreach (var lpFilter in LowPassFilters)
        {
            lpFilter.cutoffFrequency = lpf_FreqCut;
            lpFilter.lowpassResonanceQ = lpf_Resonance;
        }
        
        // Disable all surface booleans.
        SurfaceChanged();       
    }

    // Disables All Surface Bools 
    private void SurfaceChanged()
    {
        isWoodEnabled = false;
        isCarpetEnabled = false;
        isConcreteEnabled = false;
        isDirtEnabled = false;
        isGravelBrightEnabled = false;
        isGravelDarkEnabled = false;
        isGrassEnabled = false;
        isIceEnabled = false;
        isSnowShallowEnabled = false;
        isSnowDeepEnabled = false;
        isSandDeepEnabled = false;
        isSandShallowEnabled = false;
        isSandWetEnabled = false;
        isRockEnabled = false;
        isMetalDarkEnabled = false;
        isMetalBrightEnabled = false;
        isMetalSoftEnabled = false;
        isMudEnabled = false;
        isWaterEnabled = false;
        isTileBrightEnabled = false;
        isTileDarkEnabled = false;
        isCustom1Enabled = false;
        isCustom2Enabled = false;
        isCustom3Enabled = false;
        isCustom4Enabled = false;

        if (isWoodEnabled && isCarpetEnabled && isConcreteEnabled && isDirtEnabled && isGlassEnabled
            && isGravelBrightEnabled && isGravelDarkEnabled && isIceEnabled && isSnowShallowEnabled && isRockEnabled && isMetalBrightEnabled
            && isMudEnabled && isWaterEnabled && isTileBrightEnabled && isGrassEnabled && isSnowDeepEnabled && isSandDeepEnabled && isSandWetEnabled
            && isMetalDarkEnabled && isTileDarkEnabled && isCustom1Enabled && isCustom2Enabled && isCustom3Enabled && isCustom4Enabled && isMetalSoftEnabled == false)
        {
            isAllSurfacesDisabled = true;
            if (debugMode) { Debug.Log("Is all surfaces diabled: " + isAllSurfacesDisabled); }
        }

        // If Nusicance control is active.
        if (footstepNuisanceControl)
        {
            // Reset volume for nusiance controller.
            isVolSaved = false;

            // Target reached equals false.
            targetReached = false;

            // Restores original volume.
            volume = saveVolume;
        }
      
    }

    #region Footstep Surfaces

    // SURFACE AUDIO METHODS
    public void ConcreteSteps()
    {
        if (isConcreteEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Concrete: " +
                          isConcreteEnabled + " Concrete: " + isConcreteEnabled);
            }

            // Concrete surface equals true.
            isConcreteEnabled = true;
            if (debugMode) { Debug.Log("Is concrete enabled: " + isConcreteEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(concrete_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(concrete_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            concreteHeel = new List<AudioClip>(concrete_SGO.clipsOne);
            concreteToe = new List<AudioClip>(concrete_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded concrete heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = concreteHeel;
            toeClips = concreteToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with concrete clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = concreteBus;
            soundSources[1].outputAudioMixerGroup = concreteBus;
            soundSources[2].outputAudioMixerGroup = concreteBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As concrete enabled equals: " + isConcreteEnabled); }
            return; // Do nothing.
        }
    }
   

    public void WoodSteps()
    {
        if (isWoodEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Concrete: " +
                          isConcreteEnabled + " Wood: " + isWoodEnabled);
            }

            // Concrete surface equals true.
            isWoodEnabled = true;
            if (debugMode) { Debug.Log("Is wood enabled: " + isWoodEnabled); }
            
            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(wood_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(wood_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            woodHeel = new List<AudioClip>(wood_SGO.clipsOne);
            woodToe = new List<AudioClip>(wood_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded wood heel with SGO clips"); }
            
            // Overrides heel & toe list
            heelClips = woodHeel;
            toeClips = woodToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with wood clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = woodBus;
            soundSources[1].outputAudioMixerGroup = woodBus;
            soundSources[2].outputAudioMixerGroup = woodBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }

        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As wood enabled equals: " + isWoodEnabled); }
            return; // Do nothing.
        }
       
    }

    public void RockSteps()
    {
        if (isRockEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Rock: " +
                          isRockEnabled);
            }

            // Concrete surface equals true.
            isRockEnabled = true;
            if (debugMode) { Debug.Log("Is wood enabled: " + isRockEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(rock_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(rock_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            rockHeel = new List<AudioClip>(rock_SGO.clipsOne);
            rockToe = new List<AudioClip>(rock_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded rock heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = rockHeel;
            toeClips = rockToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with rock clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = rockBus;
            soundSources[1].outputAudioMixerGroup = rockBus;
            soundSources[2].outputAudioMixerGroup = rockBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As rock enabled equals: " + isRockEnabled); }
            return; // Do nothing.
        }
        
    }

    public void SandShallowSteps()
    {
        if (isSandShallowEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Sand: " + isSandShallowEnabled);
            }

            // Concrete surface equals true.
            isSandShallowEnabled = true;
            if (debugMode) { Debug.Log("Is Sand enabled: " + isSandShallowEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(sandDryShallow_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(sandDryShallow_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            sandShallowHeel = new List<AudioClip>(sandDryShallow_SGO.clipsOne);
            sandShallowToe = new List<AudioClip>(sandDryShallow_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded sand heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = sandShallowHeel;
            toeClips = sandShallowToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with sand shallow clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = sandShallowBus;
            soundSources[1].outputAudioMixerGroup = sandShallowBus;
            soundSources[2].outputAudioMixerGroup = sandShallowBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As sand shallow enabled equals: " + isSandShallowEnabled); }
            return; // Do nothing.
        }
    }
    public void SandDeepSteps()
    {
        if (isSandDeepEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Sand: " + isSandDeepEnabled);
            }

            // Concrete surface equals true.
            isSandDeepEnabled = true;
            if (debugMode) { Debug.Log("Is Sand enabled: " + isSandDeepEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(sandDryDeep_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(sandDryDeep_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            sandDeepHeel = new List<AudioClip>(sandDryDeep_SGO.clipsOne);
            sandDeepToe = new List<AudioClip>(sandDryDeep_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded sand heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = sandDeepHeel;
            toeClips = sandDeepToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with sand deep clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = sandDeepBus;
            soundSources[1].outputAudioMixerGroup = sandDeepBus;
            soundSources[2].outputAudioMixerGroup = sandDeepBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As sand deep enabled equals: " + isSandDeepEnabled); }
            return; // Do nothing.
        }
    }
    public void SandWetSteps()
    {
        if (isSandWetEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Sand wet: " + isSandWetEnabled);
            }

            // Concrete surface equals true.
            isSandWetEnabled = true;
            if (debugMode) { Debug.Log("Is Sand wet enabled: " + isSandWetEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(sandWet_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(sandWet_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            sandWetHeel = new List<AudioClip>(sandWet_SGO.clipsOne);
            sandWetToe = new List<AudioClip>(sandWet_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded sand heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = sandWetHeel;
            toeClips = sandWetToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with sand wet clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = sandWetBus;
            soundSources[1].outputAudioMixerGroup = sandWetBus;
            soundSources[2].outputAudioMixerGroup = sandWetBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As sand Wet enabled equals: " + isSandWetEnabled); }
            return; // Do nothing.
        }
    }

    public void GravelBrightSteps()
    {
        if (isGravelBrightEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Gravel: " + isGravelBrightEnabled);
            }

            // Concrete surface equals true.
            isGravelBrightEnabled = true;
            if (debugMode) { Debug.Log("Is gravel enabled: " + isGravelBrightEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(gravelBright_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(gravelBright_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }
            // Grabs audio clips from scriptable object
            gravelBrightHeel = new List<AudioClip>(gravelBright_SGO.clipsOne);
            gravelBrightToe = new List<AudioClip>(gravelBright_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded gravel heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = gravelBrightHeel;
            toeClips = gravelBrightToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with gravel clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = gravelBrightBus;
            soundSources[1].outputAudioMixerGroup = gravelBrightBus;
            soundSources[2].outputAudioMixerGroup = gravelBrightBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As gravel enabled equals: " + isGravelBrightEnabled); }
            return; // Do nothing.
        }
    }
    public void GravelDarkSteps()
    {
        if (isGravelDarkEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Gravel dark: " + isGravelDarkEnabled);
            }

            // Concrete surface equals true.
            isGravelDarkEnabled = true;
            if (debugMode) { Debug.Log("Is gravel dark enabled: " + isGravelDarkEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(gravelDark_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(gravelDark_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }
            // Grabs audio clips from scriptable object
            gravelDarkHeel = new List<AudioClip>(gravelDark_SGO.clipsOne);
            gravelDarkToe = new List<AudioClip>(gravelDark_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded heel toe with gravel dark SGO clips"); }

            // Overrides heel & toe list
            heelClips = gravelDarkHeel;
            toeClips = gravelDarkToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with gravel clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = gravelDarkBus;
            soundSources[1].outputAudioMixerGroup = gravelDarkBus;
            soundSources[2].outputAudioMixerGroup = gravelDarkBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As gravel enabled equals: " + isGravelDarkEnabled); }
            return; // Do nothing.
        }
    }
    public void DirtSteps()
    {
        if (isDirtEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Dirt: " + isDirtEnabled);
            }

            // Concrete surface equals true.
            isDirtEnabled = true;
            if (debugMode) { Debug.Log("Is dirt enabled: " + isDirtEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(dirt_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(dirt_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            dirtHeel = new List<AudioClip>(dirt_SGO.clipsOne);
            dirtToe = new List<AudioClip>(dirt_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded dirt heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = dirtHeel;
            toeClips = dirtToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with dirt clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = dirtBus;
            soundSources[1].outputAudioMixerGroup = dirtBus;
            soundSources[2].outputAudioMixerGroup = dirtBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As dirt enabled equals: " + isDirtEnabled); }
            return; // Do nothing.
        }
    }
    public void GrassSteps()
    {
        if (isGrassEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Grass: " + isGrassEnabled);
            }

            // Concrete surface equals true.
            isGrassEnabled = true;
            if (debugMode) { Debug.Log("Is grass enabled: " + isGrassEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(grass_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(grass_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            grassHeel = new List<AudioClip>(grass_SGO.clipsOne);
            grassToe = new List<AudioClip>(grass_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded grass heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = grassHeel;
            toeClips = grassToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with grass clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = grassBus;
            soundSources[1].outputAudioMixerGroup = grassBus;
            soundSources[2].outputAudioMixerGroup = grassBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As grass enabled equals: " + isGrassEnabled); }
            return; // Do nothing.
        }
    }
   public void TileBrightSteps()
    {
        if (isTileBrightEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Tile: " + isTileBrightEnabled);
            }

            // Concrete surface equals true.
            isDirtEnabled = true;
            if (debugMode) { Debug.Log("Is tile enabled: " + isTileBrightEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(tileBright_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(tileBright_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            tileBrightHeel = new List<AudioClip>(tileBright_SGO.clipsOne);
            tileBrightToe = new List<AudioClip>(tileBright_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded tile heel with SGO clips"); }

            // Overrides heel & toe list
            heelClips = tileBrightHeel;
            toeClips = tileBrightToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with tile clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = tileBrightBus;
            soundSources[1].outputAudioMixerGroup = tileBrightBus;
            soundSources[2].outputAudioMixerGroup = tileBrightBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As tile enabled equals: " + isTileBrightEnabled); }
            return; // Do nothing.
        }
    }
    public void TileDarkSteps()
    {
        if (isTileDarkEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Tile Dark: " + isTileDarkEnabled);
            }

            // Concrete surface equals true.
            isDirtEnabled = true;
            if (debugMode) { Debug.Log("Is tile dark enabled: " + isTileDarkEnabled); }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode) { Debug.Log("Is active clips stored" + isActiveClipsStored); }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(tileDark_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(tileDark_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            tileDarkHeel = new List<AudioClip>(tileDark_SGO.clipsOne);
            tileDarkToe = new List<AudioClip>(tileDark_SGO.clipsTwo);
            if (debugMode) { Debug.Log("Loaded heel toe with tile dark SGO clips"); }

            // Overrides heel & toe list
            heelClips = tileDarkHeel;
            toeClips = tileDarkToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with tile dark clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = tileDarkBus;
            soundSources[1].outputAudioMixerGroup = tileDarkBus;
            soundSources[2].outputAudioMixerGroup = tileDarkBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As tile enabled equals: " + isTileDarkEnabled); }
            return; // Do nothing.
        }
    }

    public void WaterSteps()
    {
        if (isWaterEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Water: " + isWaterEnabled);
            }

            // Concrete surface equals true.
            isWaterEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is water enabled: " + isWaterEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(water_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(water_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            waterHeel = new List<AudioClip>(water_SGO.clipsOne);
            waterToe = new List<AudioClip>(water_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded water heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = waterHeel;
            toeClips = waterToe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with water clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = waterBus;
            soundSources[1].outputAudioMixerGroup = waterBus;
            soundSources[2].outputAudioMixerGroup = waterBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As water enabled equals: " + isWaterEnabled); }
            return; // Do nothing.
        }
    }
    public void CarpetSteps()
    {
        if (isCarpetEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Carpet: " + isCarpetEnabled);
            }

            // Concrete surface equals true.
            isCarpetEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is carpet enabled: " + isCarpetEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(carpet_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(carpet_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            carpetHeel = new List<AudioClip>(carpet_SGO.clipsOne);
            carpetToe = new List<AudioClip>(carpet_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded carpet heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = carpetHeel;
            toeClips = carpetToe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with tile clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = carpetBus;
            soundSources[1].outputAudioMixerGroup = carpetBus;
            soundSources[2].outputAudioMixerGroup = carpetBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As carpet enabled equals: " + isCarpetEnabled); }
            return; // Do nothing.
        }
    }
    public void IceSteps()
    {
        if (isIceEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Ice: " + isIceEnabled);
            }

            // Concrete surface equals true.
            isIceEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is ice enabled: " + isIceEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(ice_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(ice_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            iceHeel = new List<AudioClip>(ice_SGO.clipsOne);
            iceToe = new List<AudioClip>(ice_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded ice heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = iceHeel;
            toeClips = iceToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with ice clips");}

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = iceBus;
            soundSources[1].outputAudioMixerGroup = iceBus;
            soundSources[2].outputAudioMixerGroup = iceBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }

        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As ice enabled equals: " + isIceEnabled); }
            return; // Do nothing.
        }
    }
    public void SnowShallowSteps()
    {
        if (isSnowShallowEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Snow Shallow: " + isSnowShallowEnabled);
            }

            // Concrete surface equals true.
            isSnowShallowEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is snow enabled: " + isSnowShallowEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(snowShallow_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(snowShallow_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            snowShallowHeel = new List<AudioClip>(snowShallow_SGO.clipsOne);
            snowShallowToe = new List<AudioClip>(snowShallow_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded snow heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = snowShallowHeel;
            toeClips = snowShallowToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with snow clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = snowShallowBus;
            soundSources[1].outputAudioMixerGroup = snowShallowBus;
            soundSources[2].outputAudioMixerGroup = snowShallowBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As snow shallow enabled equals: " + isSnowShallowEnabled); }
            return; // Do nothing.
        }

    }
    public void SnowDeepSteps()
    {
        if (isSnowDeepEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Snow Deep: " + isSnowDeepEnabled);
            }

            // Concrete surface equals true.
            isSnowDeepEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is snow enabled: " + isSnowDeepEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(snowDeep_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(snowDeep_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            snowDeepHeel = new List<AudioClip>(snowDeep_SGO.clipsOne);
            snowDeepToe = new List<AudioClip>(snowDeep_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded snow heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = snowShallowHeel;
            toeClips = snowShallowToe;
            if (debugMode) { Debug.Log("Overridden heel/toe clips with snow clips"); }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = snowDeepBus;
            soundSources[1].outputAudioMixerGroup = snowDeepBus;
            soundSources[2].outputAudioMixerGroup = snowDeepBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As snow deep enabled equals: " + isSnowDeepEnabled); }
            return; // Do nothing.
        }

    }


    public void MudSteps()
    {
        if (isMudEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Mud: " + isMudEnabled);
            }

            // <ud surface equals true.
            isMudEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is mud enabled: " + isMudEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(mud_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(mud_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            mudHeel = new List<AudioClip>(mud_SGO.clipsOne);
            mudToe = new List<AudioClip>(mud_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded mud heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = mudHeel;
            toeClips = mudToe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with mud clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = mudBus;
            soundSources[1].outputAudioMixerGroup = mudBus;
            soundSources[2].outputAudioMixerGroup = mudBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As mud enabled equals: " + isMudEnabled); }
            return; // Do nothing.
        }
    }

    public void MetalBrightSteps()
    {
        if (isMetalBrightEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Metal Bright: " + isMetalBrightEnabled);
            }

            // <ud surface equals true.
            isMetalBrightEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is metal bright enabled: " + isMetalBrightEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(metalBright_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(metalBright_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            metalBrightHeel = new List<AudioClip>(metalBright_SGO.clipsOne);
            metalBrightToe = new List<AudioClip>(metalBright_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded mud heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = metalBrightHeel;
            toeClips = metalBrightToe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with metal bright clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = metalBrightBus;
            soundSources[1].outputAudioMixerGroup = metalBrightBus;
            soundSources[2].outputAudioMixerGroup = metalBrightBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As metal bright enabled equals: " + isMetalBrightEnabled); }
            return; // Do nothing.
        }
    }

    public void MetalDarkSteps()
    {
        if (isMetalDarkEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Metal Dark: " + isMetalDarkEnabled);
            }

            // <ud surface equals true.
            isMetalDarkEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is Metal Dark enabled: " + isMetalDarkEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(metalDark_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(metalDark_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            metalDarkHeel = new List<AudioClip>(metalDark_SGO.clipsOne);
            metalDarkToe = new List<AudioClip>(metalDark_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded metal dark heel with SGO clips");
            }


            // Overrides heel & toe list
            heelClips = metalDarkHeel;
            toeClips = metalDarkToe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with metal clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = metalDarkBus;
            soundSources[1].outputAudioMixerGroup = metalDarkBus;
            soundSources[2].outputAudioMixerGroup = metalDarkBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As metal dark enabled equals: " + isMetalDarkEnabled); }
            return; // Do nothing.
        }
    }

    public void MetalSoftSteps()
    {
        if (isMetalSoftEnabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Metal Soft: " + isMetalSoftEnabled);
            }

            //  surface equals true.
            isMetalSoftEnabled = true;
            if (debugMode)
            {
                Debug.Log("Is Metal Soft enabled: " + isMetalSoftEnabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(metalSoft_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(metalSoft_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            metalSoftHeel = new List<AudioClip>(metalSoft_SGO.clipsOne);
            metalSoftToe = new List<AudioClip>(metalSoft_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded metal heel with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = metalSoftHeel;
            toeClips = metalSoftToe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with metal soft clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = metalSoftBus;
            soundSources[1].outputAudioMixerGroup = metalSoftBus;
            soundSources[2].outputAudioMixerGroup = metalSoftBus;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As metal soft enabled equals: " + isMetalSoftEnabled); }
            return; // Do nothing.
        }
    }

    public void Custom1Steps()
    {
        if (isCustom1Enabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Custom: " + isCustom1Enabled);
            }

            //  surface equals true.
            isCustom1Enabled = true;
            if (debugMode)
            {
                Debug.Log("Is Custom enabled: " + isCustom1Enabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(custom1_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(custom1_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            custom1Heel = new List<AudioClip>(custom1_SGO.clipsOne);
            custom1Toe = new List<AudioClip>(custom1_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded custom with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = custom1Heel;
            toeClips = custom1Toe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with custom clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = customBus1;
            soundSources[1].outputAudioMixerGroup = customBus1;
            soundSources[2].outputAudioMixerGroup = customBus1;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As custom enabled equals: " + isCustom1Enabled); }
            return; // Do nothing.
        }
    }

    public void Custom2Steps()
    {
        if (isCustom2Enabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Custom: " + isCustom2Enabled);
            }

            //  surface equals true.
            isCustom2Enabled = true;
            if (debugMode)
            {
                Debug.Log("Is Custom enabled: " + isCustom2Enabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(custom2_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(custom2_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            custom2Heel = new List<AudioClip>(custom2_SGO.clipsOne);
            custom2Toe = new List<AudioClip>(custom2_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded custom with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = custom2Heel;
            toeClips = custom2Toe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with custom clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = customBus2;
            soundSources[1].outputAudioMixerGroup = customBus2;
            soundSources[2].outputAudioMixerGroup = customBus2;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As custom enabled equals: " + isCustom2Enabled); }
            return; // Do nothing.
        }
    }

    public void Custom3Steps()
    {
        if (isCustom3Enabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Custom: " + isCustom3Enabled);
            }

            //  surface equals true.
            isCustom3Enabled = true;
            if (debugMode)
            {
                Debug.Log("Is Custom enabled: " + isCustom3Enabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(custom3_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(custom3_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            custom3Heel = new List<AudioClip>(custom3_SGO.clipsOne);
            custom3Toe = new List<AudioClip>(custom3_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded custom with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = custom3Heel;
            toeClips = custom3Toe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with custom clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = customBus3;
            soundSources[1].outputAudioMixerGroup = customBus3;
            soundSources[2].outputAudioMixerGroup = customBus3;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As custom enabled equals: " + isCustom3Enabled); }
            return; // Do nothing.
        }
    }

    public void Custom4Steps()
    {
        if (isCustom4Enabled == false)
        {
            // All surface types equal false.
            SurfaceChanged();
            if (debugMode)
            {
                Debug.Log("All surfaces disabled method.  Custom: " + isCustom4Enabled);
            }

            //  surface equals true.
            isCustom4Enabled = true;
            if (debugMode)
            {
                Debug.Log("Is Custom enabled: " + isCustom4Enabled);
            }

            // Active clips stored equals false.  
            isActiveClipsStored = false;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored);
            }

            // Stores a hard copy of the audio clips.
            storeHeelClips = new List<AudioClip>(custom4_SGO.clipsOne);
            storeToeClips = new List<AudioClip>(custom4_SGO.clipsTwo);
            isActiveClipsStored = true;
            if (debugMode)
            {
                Debug.Log("Is active clips stored" + isActiveClipsStored
                                                   + "Total heel clips stored: " + storeHeelClips.Count
                                                   + "Total toe clips stored: " + storeToeClips.Count);
            }

            // Grabs audio clips from scriptable object
            custom4Heel = new List<AudioClip>(custom4_SGO.clipsOne);
            custom4Toe = new List<AudioClip>(custom4_SGO.clipsTwo);
            if (debugMode)
            {
                Debug.Log("Loaded custom with SGO clips");
            }

            // Overrides heel & toe list
            heelClips = custom4Heel;
            toeClips = custom4Toe;
            if (debugMode)
            {
                Debug.Log("Overridden heel/toe clips with custom clips");
            }

            // Send soundsources to the correct audio Bus
            soundSources[0].outputAudioMixerGroup = customBus4;
            soundSources[1].outputAudioMixerGroup = customBus4;
            soundSources[2].outputAudioMixerGroup = customBus4;
            if (debugMode) { Debug.Log("Sound Sources 0 and 1 sent to audio bus"); }
        }
        else
        {
            if (debugMode) { Debug.Log("Nothing to do. As custom enabled equals: " + isCustom4Enabled); }
            return; // Do nothing.
        }
    }

    #endregion




    // SURFACE LAYER SWEETNERS
   public void LayerReset()
    {
        isLayerGlassShatteredEnabled = false;
        isLayerWoodCreekEnabled = false;
        isLayerWaterPuddleEnabled = false;
        isLayerWaterMovementEnabled = false;
        isLayerGrassFoilageEnabled = false;

        if (debugMode) { Debug.Log("Layer Bools Reset To False"); }
        
    }


    public void PlayLayer()
    {
        if (activeLayerClips.Count == 0)
        {
            return;
        }

        // Is Layer Clips Stored.
        if (!isLayerClipsStored)
        {
            // Store clips
            storeLayerClips = new List<AudioClip>(activeLayerClips);

            // Layer clips stored equals true.
            isLayerClipsStored = true;
        }

        // If active clip count is 0
        if (activeLayerClips.Count == 0) 
        {
            // Refill active clips with stored layer clips.
            activeLayerClips = new List<AudioClip>(storeLayerClips);
        }

        if (soundSources[3].isPlaying)
        {
            if (debugMode) { Debug.Log("Layer is already playing");}

            // If Layer is puddle or glass source is already playing, then play through alternative audio source.
            if (isLayerWaterPuddleEnabled || isLayerGlassShatteredEnabled)
            {
                // Count clips in layer.
                int clipCount = activeLayerClips.Count;

                // Random index equals random number between 0 and clipcount.
                int randomIndex = Random.Range(0, clipCount);

                // Loads clip to play.
                soundSources[4].clip = activeLayerClips[randomIndex];

                // Plays clip.
                soundSources[4].Play();

                // Removes clip from audio list.
                activeLayerClips.RemoveAt(randomIndex);

                if (debugMode) { Debug.Log("Played layer"); }
            }

            return;
        }
        // Play through original audio source.
        else
        {
            // Count clips in layer.
            int clipCount = activeLayerClips.Count;

            // Random index equals random number between 0 and clipcount.
            int randomIndex = Random.Range(0, clipCount);

            // Loads clip to play.
            soundSources[3].clip = activeLayerClips[randomIndex];

            // Plays clip.
            soundSources[3].Play();

            // Removes clip from audio list.
            activeLayerClips.RemoveAt(randomIndex);

            if (debugMode) { Debug.Log("Played layer"); }
        }


    }

    public void GrassFoliage()
    {
        if (!isLayerGrassFoilageEnabled)
        {
            // Assign the grass clips to grass foilage.
            grassFoilage = layer_grassFoliage_SGO.clipsOne;
            
            // Creates a new copy of the list.
            activeLayerClips = new List<AudioClip>(grassFoilage);

            // Layer clips equals false.
            isLayerClipsStored = false;

            // Switches all layer bools to false.
            LayerReset();

            // Grass Foilage equals true.
            isLayerGrassFoilageEnabled = true;

            // Reroute audio source to correct bus..
            soundSources[3].outputAudioMixerGroup = layerGrassFoliageBus;
            soundSources[4].outputAudioMixerGroup = layerGrassFoliageBus;

        }
        else
        {
            return;
        }

      
        if (debugMode) { Debug.Log("Long Grass Layer"); }
    }
   
    public void GlassShattered()
    {
        if (!isLayerGlassShatteredEnabled)
        {
            // Assign the grass clips to grass foilage.
            glassShattered = layer_glassShattered_SGO.clipsOne;

            // Creates a new copy of the list.
            activeLayerClips = new List<AudioClip>(glassShattered);

            // Layer clips equals false.
            isLayerClipsStored = false;

            // Switches all layer bools to false.
            LayerReset();

            // Grass Foilage equals true.
            isLayerGlassShatteredEnabled = true;

            // Reroute audio source to correct bus..
            soundSources[3].outputAudioMixerGroup = layerGlassShatteredBus;
            soundSources[4].outputAudioMixerGroup = layerGrassFoliageBus;

        }
        else
        {
            return;
        }


        if (debugMode) { Debug.Log("Glass Shattered Layer"); }
    }
    public void WaterMovement()
    {
        if (!isLayerWaterMovementEnabled)
        {
            // Assign the grass clips to grass foilage.
            water = layer_waterMovement_SGO.clipsOne;

            // Creates a new copy of the list.
            activeLayerClips = new List<AudioClip>(water);

            // Layer clips equals false.
            isLayerClipsStored = false;

            // Switches all layer bools to false.
            LayerReset();

            // Grass Foilage equals true.
            isLayerWaterMovementEnabled = true;

            // Reroute audio source to correct bus..
            soundSources[3].outputAudioMixerGroup = layerWaterMovementBus;
            soundSources[4].outputAudioMixerGroup = layerGrassFoliageBus;

        }
        else
        {
            return;
        }

        if (debugMode) { Debug.Log("Water Layer"); }
    }

    public void WaterPuddle()
    {
        if (!isLayerWaterPuddleEnabled)
        {
            // Assign the grass clips to grass foilage.
            waterPuddle = layer_waterPuddle_SGO.clipsOne;

            // Creates a new copy of the list.
            activeLayerClips = new List<AudioClip>(waterPuddle);

            // Layer clips equals false.
            isLayerClipsStored = false;

            // Switches all layer bools to false.
            LayerReset();

            // Grass Foilage equals true.
            isLayerWaterPuddleEnabled = true;

            // Reroute audio source to correct bus..
            soundSources[3].outputAudioMixerGroup = layerWaterPuddleBus;
            soundSources[4].outputAudioMixerGroup = layerGrassFoliageBus;

        }
        else
        {
            return;
        }


        if (debugMode) { Debug.Log("Water Puddle Layer"); }
    }

    public void WoodCreek()
    {
        if (!isLayerWoodCreekEnabled)
        {
            // Assign the grass clips to grass foilage.
            woodCreek = layer_woodCreek_SGO.clipsOne;

            // Creates a new copy of the list.
            activeLayerClips = new List<AudioClip>(woodCreek);

            // Layer clips equals false.
            isLayerClipsStored = false;

            // Switches all layer bools to false.
            LayerReset();

            // Grass Foilage equals true.
            isLayerWoodCreekEnabled = true;

            // Reroute audio source to correct bus..
            soundSources[3].outputAudioMixerGroup = layerWoodCreekBus;
            soundSources[4].outputAudioMixerGroup = layerGrassFoliageBus;

        }
        else
        {
            return;
        }

      
        if (debugMode) { Debug.Log("Wood Creek Layer"); }
        
    }

    public void CharacterWetFeet()
    {
       // Store Clips
        if (!isCharacterWetClipsStored)
        {
            storeCharacterWetClips = new List<AudioClip>(layer_waterPuddle_SGO.clipsOne);
            Debug.Log("Wet clips stored");
            isCharacterWetClipsStored = true;

            
            // Assigns active clips with a copy of the clips.
            activeCharacterWetClips = new List<AudioClip>(storeCharacterWetClips);

            // Assigns Audio Buses
            soundSources[5].outputAudioMixerGroup = layerCharacterWetFeetBus;
            soundSources[6].outputAudioMixerGroup = layerCharacterWetFeetBus;

        }

        // Refills clip audio pool if it empties.
        if (activeCharacterWetClips.Count == 0)
        {
            activeCharacterWetClips = new List<AudioClip>(storeCharacterWetClips);
        }

        // Count clips in layer.
        int clipCount = activeCharacterWetClips.Count;

        // Random index equals random number between 0 and clipcount.
        int randomIndex = Random.Range(0, clipCount);

        // Loads clip to play.
        soundSources[5].clip = activeCharacterWetClips[randomIndex];

        // Plays clip.
        soundSources[5].Play();

        // Removes clip from audio list.
        activeCharacterWetClips.RemoveAt(randomIndex);

        if (debugMode) { Debug.Log("Played layer"); }





    }

   

}