﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEditor;
using UnityEngine;
using UnityEngine.Audio;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class CharacterEquipped : MonoBehaviour
{
    public bool debugMode;

    // Mix Buses
    public AudioMixerGroup cargoBus;
    public AudioMixerGroup leatherBus;
    public AudioMixerGroup denimBus;
    public AudioMixerGroup waterproofBus;
    public AudioMixerGroup chainmailBus;
    public AudioMixerGroup platedBus;

    public AudioMixerGroup gunBus;
    public AudioMixerGroup swordBus;
    public AudioMixerGroup metallicBus;
    public AudioMixerGroup backpackBus;



    // Lower Body Audio Clips
    public List<AudioClip> lbClothingSounds1;
    public List<AudioClip> lbClothingSounds2;
    public List<AudioClip> lbEquipmentSounds1;
    public List<AudioClip> lbEquipmentSounds2;

    public List<AudioClip> lbClothingSounds1B;
    public List<AudioClip> lbClothingSounds2B;
    public List<AudioClip> lbEquipmentSounds1B;
    public List<AudioClip> lbEquipmentSounds2B;

    // Lower clothing wet 1.
    public List<AudioClip> lbClothingWet;


    public float lbClothing1Delay;
    public float lbClothing2Delay;
    public float lbEquipment1Delay;
    public float lbEquipment2Delay;


    // Save Audio Clips
    public List<AudioClip> storeClothing1Sounds;
    public List<AudioClip> storeClothing2Sounds;


    private bool isLowClothing1Active;
    private bool isLowClothing2Active;

    // Upper Body Audio Clips
    public List<AudioClip> ubClothingSounds1;
    public List<AudioClip> ubClothingSounds2;
    public List<AudioClip> ubEquipmentSounds1;
    public List<AudioClip> ubEquipmentSounds2;

    public List<AudioClip> ubClothingSounds1B;
    public List<AudioClip> ubClothingSounds2B;
    public List<AudioClip> ubEquipmentSounds1B;
    public List<AudioClip> ubEquipmentSounds2B;

    // Clothing wet layer.
    public List<AudioClip> ubClothingWet;



    private int indexSelect;
    private int indexTotal;

    // Clothing Booleans
    private bool isLowClothing1None;
    private bool isLowClothing1Leather;
    private bool isLowClothing1Cargo;
    private bool isLowClothing1Denim;
    private bool isLowClothing1Waterproof;
    private bool isLowClothing1Chainmail;
    private bool isLowClothing1Plated;

    private bool isLowClothing2None;
    private bool isLowClothing2Leather;
    private bool isLowClothing2Cargo;
    private bool isLowClothing2Denim;
    private bool isLowClothing2Waterproof;
    private bool isLowClothing2Chainmail;
    private bool isLowClothing2Plated;

    private bool isUpperClothing1None;
    private bool isUpperClothing1Leather;
    private bool isUpperClothing1Cargo;
    private bool isUpperClothing1Denim;
    private bool isUpperClothing1Waterproof;
    private bool isUpperClothing1Chainmail;
    private bool isUpperClothing1Plated;

    private bool isUpperClothing2None;
    private bool isUpperClothing2Leather;
    private bool isUpperClothing2Cargo;
    private bool isUpperClothing2Denim;
    private bool isUpperClothing2Waterproof;
    private bool isUpperClothing2Chainmail;
    private bool isUpperClothing2Plated;

    // Accessory Bools
    private bool isLowAccessory1None;
    private bool isLowAccessory1Gun;
    private bool isLowAccessory1Sword;
    private bool isLowAccessory1Metallic;

    private bool isLowAccessory2None;
    private bool isLowAccessory2Gun;
    private bool isLowAccessory2Sword;
    private bool isLowAccessory2Metallic;

    private bool isUpperAccessory1None;
    private bool isUpperAccessory1Gun;
    private bool isUpperAccessory1Sword;
    private bool isUpperAccessory1Metallic;
    private bool isUpperAccessory1Backpack;

    private bool isUpperAccessory2None;
    private bool isUpperAccessory2Gun;
    private bool isUpperAccessory2Sword;
    private bool isUpperAccessory2Metallic;
    private bool isUpperAccessory2Backpack;






    // VARIABLES
    // Lower and upper body clothing types.
    public enum LowerBodyClothing
    {
        None,
        Leather,
        Cargo,
        Waterproof,
        Denim,
        Chainmail,
        Plated
    }

    public enum UpperBodyClothing
    {
        None,
        Leather,
        Cargo,
        Waterproof,
        Denim,
        Chainmail,
        Plated
    }

    // Lower and Upper body accessory types.
    public enum LowerBodyAccesories
    {
        None,
        Gun,
        Sword,
        Metallic
    }

    public enum UpperBodyAccesories
    {
        None,
        Gun,
        Sword,
        Metallic,
        Backpack
    }


    // SHOW ON INSPECTOR.
    // Equipped Clothing.
    [Header("Character Clothing Materials")]
    public LowerBodyClothing lowerBodyClothing1;

    public LowerBodyClothing lowerBodyClothing2;

    public UpperBodyClothing upperBodyClothing1;
    public UpperBodyClothing upperBodyClothing2;

    // Equipped Accessories.
    [Header("Character Accessory Sounds")] public LowerBodyAccesories lowerBodyAccesories1;
    public LowerBodyAccesories lowerBodyAccesories2;

    public UpperBodyAccesories upperBodyAccesories1;
    public UpperBodyAccesories upperBodyAccesories2;


    // Audio Variables
    public AudioSource[] soundSources;
    public List<AudioClip> lowerBodyClothingAudio1;
    public List<AudioClip> lowerBodyClothingAudio2;
    public List<AudioClip> upperBodyAccessoriesAudio1;
    public List<AudioClip> lowerBodyAccessoriesAudio2;

    // Clothing Material References To Game Object
    [Header("Lower Clothing")]
    public CharacterClothing lowLeather_SGO;
    public CharacterClothing lowDenim_SGO;
    public CharacterClothing lowCargo_SGO;
    public CharacterClothing lowWaterproof_SGO;
    public CharacterClothing lowChainmail_SGO;
    public CharacterClothing lowPlated_SGO;



    [Header("Upper Clothing")]
    public CharacterClothing upperLeather_SGO;
    public CharacterClothing upperDenim_SGO;
    public CharacterClothing upperCargo_SGO;
    public CharacterClothing upperWaterproof_SGO;
    public CharacterClothing upperChainmail_SGO;
    public CharacterClothing upperPlated_SGO;

    [Header("Wet Clothing Layer")]
    public CharacterClothing wetClothingLayer_SGO;




    // Equipment Material References To GameObject
    [Header("Upper Equipment")]
    public CharacterEquipment upperGun_SGO;
    public CharacterEquipment upperSword_SGO;
    public CharacterEquipment upperMetallic_SGO;
    public CharacterEquipment upperBackpack_SGO;

    [Header("Lower Equipment")]
    public CharacterEquipment lowGun_SGO;
    public CharacterEquipment lowSword_SGO;
    public CharacterEquipment lowMetallic_SGO;
    


    // Volume Variables
    public float volume;
    private float newVolume;
    public float randomVolume;
    private float addVolume;
    private float subVolume;
    private float minVolume;
    private float maxVolume;

    public float lowClothingVolume1;
    public float lowClothingVolume2;
    public float upperClothingVolume1;
    public float upperClothingVolume2;


    // Pitch Variables
    public float pitch;
    private float newPitch;
    public float randomPitch;
    private float subPitch;
    private float addPitch;
    private float minPitch;
    private float maxPitch;

    public float lowClothingPitch1;
    public float lowClothingPitch2;
    public float upperClothingPitch1;
    public float upperClothingPitch2;

    // Movement Variables
    public float lb_Clothing1Time;
    public float lb_Clothing2Time;
    public float ub_Clothing1Time;
    public float ub_Clothing2Time;

    // Audio Modulations 
    public bool pitchMod;
    public bool volumeMod;



    public void CharacterMovement()
    {
        // Check equipped items via enum switch.
        LowerBodyEquip();
        //UpperBodyEquip();

        // Low Character Movement Method.
        LowCharacterMovement();

        // Upper Character Movement Method.
        //UpperCharacterMovement();

        if (debugMode)
        {
            Debug.Log("Character movement called");
        }
    }

    // Sync to animation event.
    public void LowCharacterMovement()
    {
        // CLOTHING SOUND 1
        LowClothingSound1();

        // CLOTHING SOUND 2
        //LowClothingSound2();

        // ACCESSORY SOUND 1
        //LowAccessorySound1();

        // ACCESSORY SOUND 2
        //LowAccessorySound2();
    }

    public void UpperCharacterMovement()
    {
        // CLOTHING SOUND 1
       //UpperClothingSound1();

        // CLOTHING SOUND 2
       //UpperClothingSound2();

        // ACCESSORY SOUND 1
       //UpperAccessorySound1();

        // ACCESSORY SOUND 2
        //UpperAccessorySound2();
    }



    public void LowClothingSound1()
    {
        // If lower clothing sounds 1 is not equal to none.
        if (lowerBodyClothing1 != LowerBodyClothing.None)
        {
            // Method called confirmation
            if (debugMode) { Debug.Log("Low clothing 1 is active"); }

            // If clothing 1 sounds is equal to 0.
            if (lbClothingSounds1.Count == 0)
            {
                // For each clip in clothing sounds 1 B. 
                foreach (var clip in lbClothingSounds1B)
                {
                    // Add to clothing sounds 1.
                    lbClothingSounds1.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to clothing sounds 1");
                    }
                }

                // Removes all sounds from the 2nd list.
                lbClothingSounds1B.Clear();
            }

            // Calculates the total number of sounds in the list.
            indexTotal = lbClothingSounds1.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[0].clip = lbClothingSounds1[indexSelect];

            // Volume modulation.
            if (volumeMod)
            {
                VolumeRandomise(soundSources[0]);
            }

            // Pitch modulation.
            if (pitchMod)
            {
                PitchRandomise(soundSources[0]);
            }

            // Play sound.
            soundSources[0].Play();
            // Adds the current audio clip to a new sound list.
            lbClothingSounds1B.Add(lbClothingSounds1[indexSelect]);
            // Removes played audio clip from the list.
            lbClothingSounds1.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Clothing 1 is inactive");
            }
        }

    }

    public void LowClothingSound2()
    {
        // If lower clothing sounds 2 is not equal to none.
        if (!isLowClothing2None)
        {
            // If clothing 2 sounds is equal to 0.
            if (lbClothingSounds2.Count == 0)
            {
                // For each clip in clothing sounds 2 B. 
                foreach (var clip in lbClothingSounds2B)
                {
                    // Add to clothing sounds 2.
                    lbClothingSounds2.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to clothing sounds 2");
                    }
                }

                // Removes all sounds from the 2nd list.
                lbClothingSounds2B.Clear();
            }


            // Calculates the total number of sounds in the list.
            indexTotal = lbClothingSounds2.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[1].clip = lbClothingSounds2[indexSelect];

            // Volume modulation. // Could also send unique float.
            if (volumeMod) { VolumeRandomise(soundSources[1]); }
            // Pitch modulation. // Could also send unique float.
            if (pitchMod) { PitchRandomise(soundSources[1]); }

            // Play sound.
            soundSources[1].Play();
            // Adds the current audio clip to a new sound list.
            lbClothingSounds2B.Add(lbClothingSounds2[indexSelect]);
            // Removes played audio clip from the list.
            lbClothingSounds2.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Clothing 2 is inactive");
            }
        }



    }

    public void LowAccessorySound1()
    {
        // If lower equipment sounds 1 is not equal to none.
        if (!isLowAccessory1None)
        {
            // If equipment sounds 1 is equal to 0.
            if (lbEquipmentSounds1.Count == 0)
            {
                // For each clip in clothing sounds 2 B. 
                foreach (var clip in lbEquipmentSounds1B)
                {
                    // Add to equipment sounds 1.
                    lbEquipmentSounds1.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to equipment sounds 1");
                    }
                }
                // Removes all sounds from the 2nd list.
                lbEquipmentSounds1B.Clear();
            }

            // Calculates the total number of sounds in the list.
            indexTotal = lbEquipmentSounds1.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[2].clip = lbEquipmentSounds1[indexSelect];

            // Volume modulation.
            if (volumeMod)
            {
                VolumeRandomise(soundSources[2]);
            }

            // Pitch modulation.
            if (pitchMod)
            {
                PitchRandomise(soundSources[2]);
            }

            // Play sound.
            soundSources[2].Play();
            // Adds the current audio clip to a new sound list.
            lbEquipmentSounds1B.Add(lbEquipmentSounds1[indexSelect]);
            // Removes played audio clip from the list.
            lbEquipmentSounds1.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Equipment 1 is set to none");
            }
        }
    }

    public void LowAccessorySound2()
    {
        // If lower equipment sounds 1 is not equal to none.
        if (!isLowAccessory2None)
        {
            // If equipment sounds 1 is equal to 0.
            if (lbEquipmentSounds2.Count == 0)
            {
                // For each clip in clothing sounds 2 B. 
                foreach (var clip in lbEquipmentSounds2B)
                {
                    // Add to equipment sounds 1.
                    lbEquipmentSounds2.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to equipment sounds 2");
                    }
                }
                // Removes all sounds from the 2nd list.
                lbEquipmentSounds2B.Clear();
            }

            // Calculates the total number of sounds in the list.
            indexTotal = lbEquipmentSounds2.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[3].clip = lbEquipmentSounds2[indexSelect];

            // Volume modulation.
            if (volumeMod) { VolumeRandomise(soundSources[3]); }

            // Pitch modulation.
            if (pitchMod) { PitchRandomise(soundSources[3]); }

            // Play sound.
            soundSources[3].Play();
            // Adds the current audio clip to a new sound list.
            lbEquipmentSounds2B.Add(lbEquipmentSounds2[indexSelect]);
            // Removes played audio clip from the list.
            lbEquipmentSounds2.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Equipment 2 is set to none");
            }
        }
    }

    public void UpperClothingSound1()
    {
        // If Upper clothing sounds 1 is not equal to none.
        if (upperBodyClothing1 != UpperBodyClothing.None)
        {
            // Method called confirmation
            if (debugMode) { Debug.Log("Upper clothing 1 is active"); }

            // If clothing 1 sounds is equal to 0.
            if (ubClothingSounds1.Count == 0)
            {
                // For each clip in clothing sounds 1 B. 
                foreach (var clip in ubClothingSounds1B)
                {
                    // Add to clothing sounds 1.
                    ubClothingSounds1.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to clothing sounds 1");
                    }
                }

                // Removes all sounds from the 2nd list.
                ubClothingSounds1B.Clear();
            }

            // Calculates the total number of sounds in the list.
            indexTotal = ubClothingSounds1.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[4].clip = ubClothingSounds1[indexSelect];

            // Volume modulation.
            if (volumeMod)
            {
                VolumeRandomise(soundSources[4]);
            }

            // Pitch modulation.
            if (pitchMod)
            {
                PitchRandomise(soundSources[4]);
            }

            // Play sound.
            soundSources[4].Play();
            // Adds the current audio clip to a new sound list.
            ubClothingSounds1B.Add(ubClothingSounds1[indexSelect]);
            // Removes played audio clip from the list.
            ubClothingSounds1.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Upper Clothing 1 is inactive");
            }
        }

    }

    public void UpperClothingSound2()
    {
        // If Upper clothing sounds 2 is not equal to none.
        if (!isUpperClothing2None)
        {
            // If clothing 2 sounds is equal to 0.
            if (ubClothingSounds2.Count == 0)
            {
                // For each clip in clothing sounds 2 B. 
                foreach (var clip in ubClothingSounds2B)
                {
                    // Add to clothing sounds 2.
                    ubClothingSounds2.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to clothing sounds 2");
                    }
                }

                // Removes all sounds from the 2nd list.
                lbClothingSounds2B.Clear();
            }


            // Calculates the total number of sounds in the list.
            indexTotal = ubClothingSounds2.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[5].clip = ubClothingSounds2[indexSelect];

            // Volume modulation. // Could also send unique float.
            if (volumeMod) { VolumeRandomise(soundSources[5]); }
            // Pitch modulation. // Could also send unique float.
            if (pitchMod) { PitchRandomise(soundSources[5]); }

            // Play sound.
            soundSources[5].Play();
            // Adds the current audio clip to a new sound list.
            ubClothingSounds2B.Add(ubClothingSounds2[indexSelect]);
            // Removes played audio clip from the list.
            ubClothingSounds2.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Upper Clothing 2 is set to none");
            }
        }



    }

    public void UpperAccessorySound1()
    {
        // If upper equipment sounds 1 is not equal to none.
        if (!isUpperAccessory1None)
        {
            // If equipment sounds 1 is equal to 0.
            if (ubEquipmentSounds1.Count == 0)
            {
                // For each clip in clothing sounds 2 B. 
                foreach (var clip in ubEquipmentSounds1B)
                {
                    // Add to equipment sounds 1.
                    ubEquipmentSounds1.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to equipment sounds 1");
                    }
                }
                // Removes all sounds from the 2nd list.
                ubEquipmentSounds1B.Clear();
            }

            // Calculates the total number of sounds in the list.
            indexTotal = ubEquipmentSounds1.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[6].clip = ubEquipmentSounds1[indexSelect];

            // Volume modulation.
            if (volumeMod)
            {
                VolumeRandomise(soundSources[6]);
            }

            // Pitch modulation.
            if (pitchMod)
            {
                PitchRandomise(soundSources[6]);
            }

            // Play sound.
            soundSources[6].Play();
            // Adds the current audio clip to a new sound list.
            ubEquipmentSounds1B.Add(ubEquipmentSounds1[indexSelect]);
            // Removes played audio clip from the list.
            ubEquipmentSounds1.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Upper Equipment 1 is set to none");
            }
        }
    }

    public void UpperAccessorySound2()
    {
        // If lower equipment sounds 1 is not equal to none.
        if (!isUpperAccessory2None)
        {
            // If equipment sounds 1 is equal to 0.
            if (ubEquipmentSounds2.Count == 0)
            {
                // For each clip in clothing sounds 2 B. 
                foreach (var clip in ubEquipmentSounds2B)
                {
                    // Add to equipment sounds 1.
                    ubEquipmentSounds2.Add(clip);

                    if (debugMode)
                    {
                        Debug.Log("Clip added to equipment sounds 2");
                    }
                }
                // Removes all sounds from the 2nd list.
                ubEquipmentSounds2B.Clear();
            }

            // Calculates the total number of sounds in the list.
            indexTotal = ubEquipmentSounds2.Count;
            // Assigns a random index beteen 0 and the total number of sounds in the list.
            indexSelect = Random.Range(0, indexTotal);
            // Prepare sound to play.
            soundSources[7].clip = ubEquipmentSounds2[indexSelect];

            // Volume modulation.
            if (volumeMod) { VolumeRandomise(soundSources[7]); }

            // Pitch modulation.
            if (pitchMod) { PitchRandomise(soundSources[7]); }

            // Play sound.
            soundSources[7].Play();
            // Adds the current audio clip to a new sound list.
            ubEquipmentSounds2B.Add(ubEquipmentSounds2[indexSelect]);
            // Removes played audio clip from the list.
            ubEquipmentSounds2.RemoveAt(indexSelect);
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Upper Equipment 2 is set to none");
            }
        }
    }




    void VolumeRandomise(AudioSource sound)
    {
        // Restore base value.
        newVolume = volume;
        if (debugMode)
        {
            Debug.Log("Volume = " + newVolume);
        }

        // Add volume.
        addVolume = newVolume + randomVolume;
        if (debugMode)
        {
            Debug.Log("Add Volume: = " + addVolume);
        }

        // Decrease volume.
        subVolume = newVolume - randomVolume;
        if (debugMode)
        {
            Debug.Log("Decrease Volume: = " + subVolume);
        }

        // New volume equals a random volume between volume decrease and increase.
        newVolume = Random.Range(subVolume, addVolume);
        if (debugMode)
        {
            Debug.Log("Final Volume: = " + newVolume);
        }

        // Sound instance equals new volume.  Volume clamped between min and max.  
        sound.volume = Mathf.Clamp(newVolume, minVolume, maxVolume);
    }

    void PitchRandomise(AudioSource sound)
    {
        // Restore base value.
        newPitch = pitch;

        // Add pitch.
        addPitch = newPitch + randomPitch;

        // Subtract pitch.
        subPitch = newPitch - randomPitch;

        // Randomise Pitch
        newPitch = Random.Range(subPitch, addPitch);

        // Clamp value
        sound.pitch = newPitch;

        Mathf.Clamp(sound.pitch, minPitch, maxPitch);


        if (debugMode)
        {
            Debug.Log("Pitch Soundsouce: " + sound.pitch);
        }
    }


   
    public void ResetAudio()
    {
        // Clothing sounds
        lbClothingSounds1.Clear();
        lbClothingSounds1B.Clear();
        lbClothingSounds2.Clear();
        lbClothingSounds2B.Clear();

        ubClothingSounds1.Clear();
        ubClothingSounds1B.Clear();
        ubClothingSounds2.Clear();
        ubClothingSounds2B.Clear();

        lowerBodyClothing1 = LowerBodyClothing.None;
        lowerBodyClothing2 = LowerBodyClothing.None;
        upperBodyClothing1 = UpperBodyClothing.None;
        upperBodyClothing2 = UpperBodyClothing.None;



        // Equipment sounds
        lbEquipmentSounds1.Clear();
        lbEquipmentSounds1B.Clear();
        lbEquipmentSounds2.Clear();
        lbEquipmentSounds2B.Clear();

        ubEquipmentSounds1.Clear();
        ubEquipmentSounds1B.Clear();
        ubEquipmentSounds2.Clear();
        ubEquipmentSounds2B.Clear();


        lowerBodyAccesories1 = LowerBodyAccesories.None;
        lowerBodyAccesories2 = LowerBodyAccesories.None;
        upperBodyAccesories1 = UpperBodyAccesories.None;
        upperBodyAccesories2 = UpperBodyAccesories.None;



        if (debugMode)
        {
            Debug.Log("All Sounds Cleared");
        }

    }


    ///////////////////////////////////////////////////////////////////////////
    // LOWER BODY 
    // SET CLOTHING TYPE 1
    public void LowerBodyEquip()
    {
        // Check the character clothing type 1;
        switch (lowerBodyClothing1)
        {
            case LowerBodyClothing.None:
                // if lower body clothing equal to none
                if (isLowClothing1None)
                {
                    // Lower body clothing sound 1 and 1B cleared.
                    lbClothingSounds1.Clear();
                    lbClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1 = None");
                    }

                    isLowClothing1None = true;
                    isLowClothing1Cargo = false;
                    isLowClothing1Denim = false;
                    isLowClothing1Leather = false;
                    isLowClothing1Waterproof = false;
                    isLowClothing1Chainmail = false;
                    isLowClothing1Plated = false;
                }
                else
                {
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1: Already set to none");
                    }
                }
                break;


            case LowerBodyClothing.Leather:
                // If clothing 1 leather is equal to true.
                if (!isLowClothing1Leather)
                {
                    // Lower body clothing 1 equal to leather.
                    lbClothingSounds1 = new List<AudioClip>(lowLeather_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1  = Leather");
                    }

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1B clips cleared");
                    }

                    // Set Bools
                    isLowClothing1None = false;
                    isLowClothing1Cargo = false;
                    isLowClothing1Denim = false;
                    isLowClothing1Leather = true;
                    isLowClothing1Waterproof = false;
                    isLowClothing1Chainmail = false;
                    isLowClothing1Plated = false;

                    soundSources[0].outputAudioMixerGroup = leatherBus;

                }
                else
                {
                    Debug.Log("Lower Body 1: Leather already active");
                }
                break;


            case LowerBodyClothing.Cargo:
                // If clothing 1 cargo is equal to true.
                if (!isLowClothing1Cargo)
                {
                    // Lower body clothing 1 equal to cargo.
                    lbClothingSounds1 = new List<AudioClip>(lowCargo_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1 = Low Cargo");
                    }

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1B clips cleared");
                    }

                    // Set Bools
                    isLowClothing1None = false;
                    isLowClothing1Cargo = true;
                    isLowClothing1Denim = false;
                    isLowClothing1Leather = false;
                    isLowClothing1Waterproof = false;
                    isLowClothing1Chainmail = false;
                    isLowClothing1Plated = false;

                    soundSources[0].outputAudioMixerGroup = cargoBus;
                }
                else { Debug.Log("Lower Body 1: Cargo already active"); }
                break;


            case LowerBodyClothing.Waterproof:
                if (!isLowClothing1Waterproof)
                {
                    // Lower body clothing 1 equal to waterproof.
                    lbClothingSounds1 = new List<AudioClip>(lowWaterproof_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body = Waterproof");
                    }

                    // Set Bools
                    isLowClothing1None = false;
                    isLowClothing1Cargo = false;
                    isLowClothing1Denim = false;
                    isLowClothing1Leather = false;
                    isLowClothing1Waterproof = true;
                    isLowClothing1Chainmail = false;
                    isLowClothing1Plated = false;

                    soundSources[0].outputAudioMixerGroup = waterproofBus;
                }
                else
                {
                    { Debug.Log("Lower Body 1: Waterproof already active"); }
                }
                break;

            case LowerBodyClothing.Denim:
                if (!isLowClothing1Denim)
                {
                    // Lower body clothing 1 equal to waterproof.
                    lbClothingSounds1 = new List<AudioClip>(lowDenim_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body = Denim");
                    }

                    // Set Bools
                    isLowClothing1None = false;
                    isLowClothing1Cargo = false;
                    isLowClothing1Denim = true;
                    isLowClothing1Leather = false;
                    isLowClothing1Waterproof = false;
                    isLowClothing1Chainmail = false;
                    isLowClothing1Plated = false;

                    soundSources[0].outputAudioMixerGroup = denimBus;
                }
                else
                {
                    { Debug.Log("Lower Body 1: Denim already active"); }
                }
                break;

            case LowerBodyClothing.Chainmail:
                if (!isLowClothing1Chainmail)
                {
                    // Lower body clothing 1 equal to waterproof.
                    lbClothingSounds1 = new List<AudioClip>(lowChainmail_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1 = Chainmail");
                    }

                    // Set Bools
                    isLowClothing1None = false;
                    isLowClothing1Cargo = false;
                    isLowClothing1Denim = false;
                    isLowClothing1Leather = false;
                    isLowClothing1Waterproof = false;
                    isLowClothing1Chainmail = true;
                    isLowClothing1Plated = false;

                    soundSources[0].outputAudioMixerGroup = chainmailBus;
                }
                else
                {
                    { Debug.Log("Lower Body 1: Chainmail already active"); }
                }
                break;

            case LowerBodyClothing.Plated:
                if (!isLowClothing1Plated)
                {
                    // Lower body clothing 1 equal to waterproof.
                    lbClothingSounds1 = new List<AudioClip>(lowPlated_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 1 = Low Plated");
                    }

                    // Set Bools
                    isLowClothing1None = false;
                    isLowClothing1Cargo = false;
                    isLowClothing1Denim = false;
                    isLowClothing1Leather = false;
                    isLowClothing1Waterproof = false;
                    isLowClothing1Chainmail = false;
                    isLowClothing1Plated = true;

                    soundSources[0].outputAudioMixerGroup = platedBus;
                }
                else
                {
                    { Debug.Log("Lower Body 1: Plated already active"); }
                }
                break;
        }

        ///////////////////////////////////////////////////////////////////////////
        // SET CLOTHING TYPE 2
        switch (lowerBodyClothing2)
        {
            case LowerBodyClothing.None:
                // if lower body clothing equal to none

                if (isLowClothing2None)
                {
                    // Lower body clothing sound 2 and 2B cleared.
                    lbClothingSounds2.Clear();
                    lbClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2 = None");
                    }

                    isLowClothing2None = true;
                    isLowClothing2Cargo = false;
                    isLowClothing2Denim = false;
                    isLowClothing2Leather = false;
                    isLowClothing2Waterproof = false;
                    isLowClothing2Chainmail = false;
                    isLowClothing2Plated = false;
                }
                else
                {
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2: Already set to none");
                    }
                }
                break;


            case LowerBodyClothing.Leather:
                // If clothing 2 leather is equal to true.
                if (!isLowClothing2Leather)
                {
                    // Lower body clothing 2 equal to leather.
                    lbClothingSounds2 = new List<AudioClip>(lowLeather_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2  = Leather");
                    }

                    // Clears lbClothing sounds B
                    lbClothingSounds2B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2B clips cleared");
                    }

                    // Set Bools
                    isLowClothing2None = false;
                    isLowClothing2Cargo = false;
                    isLowClothing2Denim = false;
                    isLowClothing2Leather = true;
                    isLowClothing2Waterproof = false;
                    isLowClothing2Chainmail = false;
                    isLowClothing2Plated = false;

                    soundSources[1].outputAudioMixerGroup = leatherBus;

                }
                else
                {
                    Debug.Log("Lower Body 2: Leather already active");
                }
                break;


            case LowerBodyClothing.Cargo:
                // If clothing 2 cargo is equal to true.
                if (!isLowClothing2Cargo)
                {
                    // Lower body clothing 2 equal to cargo.
                    lbClothingSounds2 = new List<AudioClip>(lowCargo_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2 = Low Cargo");
                    }

                    // Clears lbClothing sounds 2B
                    lbClothingSounds2B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2B clips cleared");
                    }

                    // Set Bools
                    isLowClothing2None = false;
                    isLowClothing2Cargo = true;
                    isLowClothing2Denim = false;
                    isLowClothing2Leather = false;
                    isLowClothing2Waterproof = false;
                    isLowClothing2Chainmail = false;
                    isLowClothing2Plated = false;

                    // Set Mixer Output
                    soundSources[1].outputAudioMixerGroup = cargoBus;

                }
                else { Debug.Log("Lower Body 2: Cargo already active"); }
                break;


            case LowerBodyClothing.Waterproof:
                if (!isLowClothing2Waterproof)
                {
                    // Lower body clothing 2 equal to waterproof.
                    lbClothingSounds2 = new List<AudioClip>(lowWaterproof_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2 = Waterproof");
                    }

                    // Set Bools
                    isLowClothing2None = false;
                    isLowClothing2Cargo = false;
                    isLowClothing2Denim = false;
                    isLowClothing2Leather = false;
                    isLowClothing2Waterproof = true;
                    isLowClothing2Chainmail = false;
                    isLowClothing2Plated = false;

                    // Set Mixer Output
                    soundSources[1].outputAudioMixerGroup = waterproofBus;
                }
                else
                {
                    { Debug.Log("Lower Body 2: Waterproof already active"); }
                }
                break;

            case LowerBodyClothing.Denim:
                if (!isLowClothing2Denim)
                {
                    // Lower body clothing 2 equal to waterproof.
                    lbClothingSounds2 = new List<AudioClip>(lowDenim_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2 = Denim");
                    }

                    // Set Bools
                    isLowClothing2None = false;
                    isLowClothing2Cargo = false;
                    isLowClothing2Denim = true;
                    isLowClothing2Leather = false;
                    isLowClothing2Waterproof = false;
                    isLowClothing2Chainmail = false;
                    isLowClothing2Plated = false;

                    // Set Mixer Output
                    soundSources[1].outputAudioMixerGroup = denimBus;
                }
                else
                {
                    { Debug.Log("Lower Body 2: Denim already active"); }
                }
                break;

            case LowerBodyClothing.Chainmail:
                if (!isLowClothing2Chainmail)
                {
                    // Lower body clothing 1 equal to waterproof.
                    lbClothingSounds2 = new List<AudioClip>(lowChainmail_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2 = Chainmail");
                    }

                    // Set Bools
                    isLowClothing2None = false;
                    isLowClothing2Cargo = false;
                    isLowClothing2Denim = false;
                    isLowClothing2Leather = false;
                    isLowClothing2Waterproof = false;
                    isLowClothing2Chainmail = true;
                    isLowClothing2Plated = false;

                    // Set Mixer Output
                    soundSources[1].outputAudioMixerGroup = chainmailBus;
                }
                else
                {
                    { Debug.Log("Lower Body 2: Chainmail already active"); }
                }
                break;

            case LowerBodyClothing.Plated:
                if (!isLowClothing2Plated)
                {
                    // Lower body clothing 1 equal to waterproof.
                    lbClothingSounds2 = new List<AudioClip>(lowPlated_SGO.soundClips);

                    // Clears lbClothing sounds B
                    lbClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body 2 = Low Plated");
                    }

                    // Set Bools
                    isLowClothing2None = false;
                    isLowClothing2Cargo = false;
                    isLowClothing2Denim = false;
                    isLowClothing2Leather = false;
                    isLowClothing2Waterproof = false;
                    isLowClothing2Chainmail = false;
                    isLowClothing2Plated = true;

                    // Set Mixer Output
                    soundSources[1].outputAudioMixerGroup = platedBus;
                }
                else
                {
                    { Debug.Log("Lower Body 2: Plated already active"); }
                }
                break;
        }


        ///////////////////////////////////////////////////////////////////////////
        // SET LOW ACCESSORIES 1
        switch (lowerBodyAccesories1)
        {
            case LowerBodyAccesories.None:
                if (isLowAccessory1None)
                {
                    // Clear active audio clips.
                    lbEquipmentSounds1.Clear();
                    lbEquipmentSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body Accesory 1: None");
                    }

                    // Set Bools
                    isLowAccessory1None = true;     // Set true.
                    isLowAccessory1Gun = false;
                    isLowAccessory1Sword = false;
                    isLowAccessory1Metallic = false;


                }
                else
                {
                    { Debug.Log("Lower Body Acessory 1: Already set to none."); }
                }
                break;

            case LowerBodyAccesories.Sword:
                // If Low Acessory 1 Large Gun Equals False
                if (!isLowAccessory1Sword)
                {
                    // Set active clips.
                    lbEquipmentSounds1 = new List<AudioClip>(upperSword_SGO.soundClips);
                    // Clear old clips.
                    lbEquipmentSounds1B.Clear();

                    // Set Bools
                    isLowAccessory1None = false;     
                    isLowAccessory1Gun = false;
                    isLowAccessory1Sword = true;     // Set true.
                    isLowAccessory1Metallic = false;

                    if (debugMode){Debug.Log("Lower Body Accesory 1: Sword");}

                    // Set Bus Output
                    soundSources[2].outputAudioMixerGroup = swordBus;

                }
                else
                {
                    { Debug.Log("Lower Body Acessory 1: Already set to Sword."); }
                }
                break;

            case LowerBodyAccesories.Gun:
                // If Low Accessory 1 Small Gun Equals False.
                if (!isLowAccessory1Gun)
                {
                    // Set active clips.
                    lbEquipmentSounds1 = new List<AudioClip>(lowGun_SGO.soundClips);
                    // Clear old clips.
                    lbEquipmentSounds1B.Clear();

                    // Set Bools
                    isLowAccessory1None = false;
                    isLowAccessory1Gun = true;    // Set true. 
                    isLowAccessory1Sword = false;    
                    isLowAccessory1Metallic = false;

                    if (debugMode) { Debug.Log("Lower Body Accesory 1: Small Gun"); }

                    // Set Bus Output
                    soundSources[2].outputAudioMixerGroup = gunBus;
                }
                else
                {
                    { Debug.Log("Lower Body Acessory 1: Already set to Small Gun."); }
                }
                break;
                
            
            case LowerBodyAccesories.Metallic:
                // If Low Accessory 1 Small Metallic Equals False.
                if (!isLowAccessory1Metallic)
                {
                    // Set active clips.
                    lbEquipmentSounds1 = new List<AudioClip>(lowMetallic_SGO.soundClips);
                    // Clear old clips.
                    lbEquipmentSounds1B.Clear();

                    // Set Bools
                    isLowAccessory1None = false;
                    isLowAccessory1Gun = false;
                    isLowAccessory1Sword = false;
                    isLowAccessory1Metallic = true;    // Set true.

                    if (debugMode) { Debug.Log("Lower Body Accesory 1: Metallic"); }

                    // Set Bus Output
                    soundSources[2].outputAudioMixerGroup = metallicBus;
                }
                else
                {
                    { Debug.Log("Lower Body Acessory 1: Already set to Metallic."); }
                }
                break;
          
        }


        ///////////////////////////////////////////////////////////////////////////
        // SET LOW ACCESSORIES 2
        switch (lowerBodyAccesories2)
        {
            case LowerBodyAccesories.None:
                if (isLowAccessory2None)
                {
                    // Clear active audio clips.
                    lbEquipmentSounds2.Clear();
                    lbEquipmentSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Lower Body Accesory 2: None");
                    }

                    // Set Bools
                    isLowAccessory2None = true;     // Set true.
                    isLowAccessory2Gun = false;
                    isLowAccessory2Sword = false;
                    isLowAccessory2Metallic = false;

                   
                }
                else
                {
                    { Debug.Log("Lower Body Acessory 2: Already set to none."); }
                }
                break;

            case LowerBodyAccesories.Sword:
                // If Low Acessory 2 Large Gun Equals False
                if (!isLowAccessory2Sword)
                {
                    // Set active clips.
                    lbEquipmentSounds2 = new List<AudioClip>(lowSword_SGO.soundClips);
                    // Clear old clips.
                    lbEquipmentSounds2B.Clear();

                    // Set Bools
                    isLowAccessory2None = false;
                    isLowAccessory2Gun = false;
                    isLowAccessory2Sword = true;     // Set true.
                    isLowAccessory2Metallic = false;
                   
                    if (debugMode) { Debug.Log("Lower Body Accesory 2: Sword"); }

                    // Set Bus Output
                    soundSources[3].outputAudioMixerGroup = swordBus;
                }
                else
                {
                    { Debug.Log("Lower Body Acessory 2: Already set to Sword."); }
                }
                break;

            case LowerBodyAccesories.Gun:
                // If Low Accessory 2 Small Gun Equals False.
                if (!isLowAccessory2Sword)
                {
                    // Set active clips.
                    lbEquipmentSounds2 = new List<AudioClip>(lowGun_SGO.soundClips);
                    // Clear old clips.
                    lbEquipmentSounds2B.Clear();

                    // Set Bools
                    isLowAccessory2None = false;
                    isLowAccessory2Gun = true;    // Set true. 
                    isLowAccessory2Sword = false;
                    isLowAccessory2Metallic = false;
                  
                    if (debugMode) { Debug.Log("Lower Body Accesory 2: Gun"); }


                    // Set Bus Output
                    soundSources[3].outputAudioMixerGroup = gunBus;
                }
                else
                {
                    { Debug.Log("Lower Body Acessory 2: Already set to Gun."); }
                }
                break;

           

            case LowerBodyAccesories.Metallic:
                // If Low Accessory 2 Small Metallic Equals False.
                if (!isLowAccessory2Metallic)
                {
                    // Set active clips.
                    lbEquipmentSounds2 = new List<AudioClip>(lowMetallic_SGO.soundClips);
                    // Clear old clips.
                    lbEquipmentSounds2B.Clear();

                    // Set Bools
                    isLowAccessory2None = false;
                    isLowAccessory2Gun = false;
                    isLowAccessory2Sword = false;
                    isLowAccessory2Metallic = true;    // Set true.
                   
                    if (debugMode) { Debug.Log("Lower Body Accesory 2: Metallic"); }


                    // Set Bus Output
                    soundSources[3].outputAudioMixerGroup = metallicBus;
                }
                else
                {
                    { Debug.Log("Lower Body Acessory 2: Already set to Metallic."); }
                }
                break;
        }
       
    }



    ///////////////////////////////////////////////////////////////////////////
    // Sets Upper Body Clothing Sounds
    public void UpperBodyEquip()
    {
        // Check the character clothing type 1;
        switch (upperBodyClothing1)
        {
            case UpperBodyClothing.None:
                // if upper body clothing equal to none
                if (!isUpperClothing1None)
                {
                    // Upper body clothing sound 1 and 1B cleared.
                    ubClothingSounds1.Clear();
                    ubClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1 = None");
                    }

                    isUpperClothing1None = true;
                    isUpperClothing1Cargo = false;
                    isUpperClothing1Denim = false;
                    isUpperClothing1Leather = false;
                    isUpperClothing1Waterproof = false;
                    isUpperClothing1Chainmail = false;
                    isUpperClothing1Plated = false;
                }
                else
                {
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1: Already set to none");
                    }
                }
                break;


            case UpperBodyClothing.Leather:
                // If clothing 1 leather is equal to true.
                if (!isUpperClothing1Leather)
                {
                    // Upper body clothing 1 equal to leather.
                    ubClothingSounds1 = new List<AudioClip>(upperLeather_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1  = Leather");
                    }

                    // Clears lbClothing sounds B
                    ubClothingSounds1B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1B clips cleared");
                    }

                    // Set Bools
                    isUpperClothing1None = false;
                    isUpperClothing1Cargo = false;
                    isUpperClothing1Denim = false;
                    isUpperClothing1Leather = true;
                    isUpperClothing1Waterproof = false;
                    isUpperClothing1Chainmail = false;
                    isUpperClothing1Plated = false;


                    // Set Bus Output
                    soundSources[4].outputAudioMixerGroup = leatherBus;
                }
                else
                {
                    Debug.Log("Upper Body 1: Leather already active");
                }
                break;


            case UpperBodyClothing.Cargo:
                // If clothing 1 cargo is equal to true.
                if (!isUpperClothing1Cargo)
                {
                    // Lower body clothing 1 equal to cargo.
                    ubClothingSounds1 = new List<AudioClip>(upperCargo_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1 = Cargo");
                    }

                    // Clears lbClothing sounds B
                    ubClothingSounds1B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1B clips cleared");
                    }

                    // Set Bools
                    isUpperClothing1None = false;
                    isUpperClothing1Cargo = true;
                    isUpperClothing1Denim = false;
                    isUpperClothing1Leather = false;
                    isUpperClothing1Waterproof = false;
                    isUpperClothing1Chainmail = false;
                    isUpperClothing1Plated = false;

                    // Set Bus Output
                    soundSources[4].outputAudioMixerGroup = cargoBus;
                }
                else { Debug.Log("Upper Body 1: Cargo already active"); }
                break;


            case UpperBodyClothing.Waterproof:
                if (!isUpperClothing1Waterproof)
                {
                    // Upper body clothing 1 equal to waterproof.
                    ubClothingSounds1 = new List<AudioClip>(upperWaterproof_SGO.soundClips);

                    // Clears lbClothing sounds B
                    ubClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body = Waterproof");
                    }

                    // Set Bools
                    isUpperClothing1None = false;
                    isUpperClothing1Cargo = false;
                    isUpperClothing1Denim = false;
                    isUpperClothing1Leather = false;
                    isUpperClothing1Waterproof = true;
                    isUpperClothing1Chainmail = false;
                    isUpperClothing1Plated = false;

                    // Set Bus Output
                    soundSources[4].outputAudioMixerGroup = waterproofBus;
                }
                else
                {
                    { Debug.Log("Upper Body 1: Waterproof already active"); }
                }
                break;

            case UpperBodyClothing.Denim:
                if (!isUpperClothing1Denim)
                {
                    // Upper body clothing 1 equal to waterproof.
                    ubClothingSounds1 = new List<AudioClip>(upperDenim_SGO.soundClips);

                    // Clears lbClothing sounds B
                    ubClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body = Denim");
                    }

                    // Set Bools
                    isUpperClothing1None = false;
                    isUpperClothing1Cargo = false;
                    isUpperClothing1Denim = true;
                    isUpperClothing1Leather = false;
                    isUpperClothing1Waterproof = false;
                    isUpperClothing1Chainmail = false;
                    isUpperClothing1Plated = false;

                    // Set Bus Output
                    soundSources[4].outputAudioMixerGroup = denimBus;
                }
                else
                {
                    { Debug.Log("Upper Body 1: Denim already active"); }
                }
                break;

            case UpperBodyClothing.Chainmail:
                if (!isUpperClothing1Chainmail)
                {
                    // Upper body clothing 1 equal to waterproof.
                    ubClothingSounds1 = new List<AudioClip>(upperChainmail_SGO.soundClips);

                    // Clears lbClothing sounds B
                    ubClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1 = Chainmail");
                    }

                    // Set Bools
                    isUpperClothing1None = false;
                    isUpperClothing1Cargo = false;
                    isUpperClothing1Denim = false;
                    isUpperClothing1Leather = false;
                    isUpperClothing1Waterproof = false;
                    isUpperClothing1Chainmail = true;
                    isUpperClothing1Plated = false;

                    // Set Bus Output
                    soundSources[4].outputAudioMixerGroup = chainmailBus;
                }
                else
                {
                    { Debug.Log("Upper Body 1: Chainmail already active"); }
                }
                break;

            case UpperBodyClothing.Plated:
                if (!isUpperClothing1Plated)
                {
                    // Upper body clothing 1 equal to waterproof.
                    ubClothingSounds1 = new List<AudioClip>(upperPlated_SGO.soundClips);

                    // Clears lbClothing sounds B
                    ubClothingSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body 1 = Plated");
                    }

                    // Set Bools
                    isUpperClothing1None = false;
                    isUpperClothing1Cargo = false;
                    isUpperClothing1Denim = false;
                    isUpperClothing1Leather = false;
                    isUpperClothing1Waterproof = false;
                    isUpperClothing1Chainmail = false;
                    isUpperClothing1Plated = true;

                    // Set Bus Output
                    soundSources[4].outputAudioMixerGroup = platedBus;
                }
                else
                {
                    { Debug.Log("Upper Body 1: Plated already active"); }
                }
                break;
        }


        ///////////////////////////////////////////////////////////////////////////
        // Check the character clothing type 2;
        switch (upperBodyClothing2)
        {
            case UpperBodyClothing.None:
                // if upper body clothing equal to none
                if (!isUpperClothing2None)
                {
                    // Upper body clothing sound 2 and 2B cleared.
                    ubClothingSounds2.Clear();
                    ubClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2 = None");
                    }

                    isUpperClothing2None = true;
                    isUpperClothing2Cargo = false;
                    isUpperClothing2Denim = false;
                    isUpperClothing2Leather = false;
                    isUpperClothing2Waterproof = false;
                    isUpperClothing2Chainmail = false;
                    isUpperClothing2Plated = false;
                }
                else
                {
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2: Already set to none");
                    }
                }
                break;


            case UpperBodyClothing.Leather:
                // If clothing 2 leather is equal to true.
                if (!isUpperClothing2Leather)
                {
                    // Upper body clothing 2 equal to leather.
                    ubClothingSounds2 = new List<AudioClip>(upperLeather_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2  = Leather");
                    }

                    // Clears ub Clothing sounds B
                    ubClothingSounds2B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2B clips cleared");
                    }

                    // Set Bools
                    isUpperClothing2None = false;
                    isUpperClothing2Cargo = false;
                    isUpperClothing2Denim = false;
                    isUpperClothing2Leather = true;
                    isUpperClothing2Waterproof = false;
                    isUpperClothing2Chainmail = false;
                    isUpperClothing2Plated = false;

                    // Set Bus Output
                    soundSources[5].outputAudioMixerGroup = leatherBus;
                }
                else
                {
                    Debug.Log("Upper Body 2: Leather already active");
                }
                break;


            case UpperBodyClothing.Cargo:
                // If clothing 2 cargo is equal to true.
                if (!isUpperClothing2Cargo)
                {
                    // Upper body clothing 2 equal to cargo.
                    ubClothingSounds2 = new List<AudioClip>(upperCargo_SGO.soundClips);
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2 = Cargo");
                    }

                    // Clears ub Clothing sounds B
                    ubClothingSounds2B.Clear();
                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2B clips cleared");
                    }

                    // Set Bools
                    isUpperClothing2None = false;
                    isUpperClothing2Cargo = true;
                    isUpperClothing2Denim = false;
                    isUpperClothing2Leather = false;
                    isUpperClothing2Waterproof = false;
                    isUpperClothing2Chainmail = false;
                    isUpperClothing2Plated = false;

                    soundSources[5].outputAudioMixerGroup = cargoBus;

                }
                else { Debug.Log("Upper Body 2: Cargo already active"); }
                break;


            case UpperBodyClothing.Waterproof:
                if (!isUpperClothing2Waterproof)
                {
                    // Upper body clothing 2 equal to waterproof.
                    ubClothingSounds2 = new List<AudioClip>(upperWaterproof_SGO.soundClips);

                    // Clears ub Clothing sounds B
                    ubClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body = Waterproof");
                    }

                    // Set Bools
                    isUpperClothing2None = false;
                    isUpperClothing2Cargo = false;
                    isUpperClothing2Denim = false;
                    isUpperClothing2Leather = false;
                    isUpperClothing2Waterproof = true;
                    isUpperClothing2Chainmail = false;
                    isUpperClothing2Plated = false;

                    soundSources[5].outputAudioMixerGroup = waterproofBus;

                }
                else
                {
                    { Debug.Log("Upper Body 2: Waterproof already active"); }
                }
                break;

            case UpperBodyClothing.Denim:
                if (!isUpperClothing2Denim)
                {
                    // Upper body clothing 2 equal to waterproof.
                    ubClothingSounds2 = new List<AudioClip>(upperDenim_SGO.soundClips);

                    // Clears lbClothing sounds B
                    ubClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body = Denim");
                    }

                    // Set Bools
                    isUpperClothing2None = false;
                    isUpperClothing2Cargo = false;
                    isUpperClothing2Denim = true;
                    isUpperClothing2Leather = false;
                    isUpperClothing2Waterproof = false;
                    isUpperClothing2Chainmail = false;
                    isUpperClothing2Plated = false;

                    soundSources[5].outputAudioMixerGroup = denimBus;

                }
                else
                {
                    { Debug.Log("Upper Body 2: Denim already active"); }
                }
                break;

            case UpperBodyClothing.Chainmail:
                if (!isUpperClothing2Chainmail)
                {
                    // Upper body clothing 2 equal to waterproof.
                    ubClothingSounds2 = new List<AudioClip>(upperChainmail_SGO.soundClips);

                    // Clears ubClothing sounds B
                    ubClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2 = Chainmail");
                    }

                    // Set Bools
                    isUpperClothing2None = false;
                    isUpperClothing2Cargo = false;
                    isUpperClothing2Denim = false;
                    isUpperClothing2Leather = false;
                    isUpperClothing2Waterproof = false;
                    isUpperClothing2Chainmail = true;
                    isUpperClothing2Plated = false;

                    soundSources[5].outputAudioMixerGroup = chainmailBus;

                }
                else
                {
                    { Debug.Log("Upper Body 2: Chainmail already active"); }
                }
                break;

            case UpperBodyClothing.Plated:
                if (!isUpperClothing2Plated)
                {
                    // Upper body clothing 2 equal to waterproof.
                    ubClothingSounds2 = new List<AudioClip>(upperPlated_SGO.soundClips);

                    // Clears lbClothing sounds B
                    ubClothingSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body 2 = Plated");
                    }

                    // Set Bools
                    isUpperClothing2None = false;
                    isUpperClothing2Cargo = false;
                    isUpperClothing2Denim = false;
                    isUpperClothing2Leather = false;
                    isUpperClothing2Waterproof = false;
                    isUpperClothing2Chainmail = false;
                    isUpperClothing2Plated = true;

                    soundSources[5].outputAudioMixerGroup = platedBus;

                }
                else
                {
                    { Debug.Log("Upper Body 2: Plated already active"); }
                }
                break;
        }

        ///////////////////////////////////////////////////////////////////////////
        // SET UPPER BODY ACCESSORIES 1
        switch (upperBodyAccesories1)
        {
            case UpperBodyAccesories.None:
                if (isUpperAccessory1None)
                {
                    // Clear active audio clips.
                    ubEquipmentSounds1.Clear();
                    ubEquipmentSounds1B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body Accesory 1: None");
                    }

                    // Set Bools
                    isUpperAccessory1None = true;     // Set true.
                    isUpperAccessory1Gun = false;
                    isUpperAccessory1Sword = false;
                    isUpperAccessory1Metallic = false;
                    isUpperAccessory1Backpack = false;

                }
                else
                {
                    { Debug.Log("Upper Body Acessory 1: Already set to none."); }
                }
                break;

            case UpperBodyAccesories.Sword:
                // If Upper Acessory 1 Sword Equals False
                if (!isUpperAccessory1Sword)
                {
                    // Set active clips.
                    ubEquipmentSounds1 = new List<AudioClip>(upperSword_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds1B.Clear();

                    // Set Bools
                    isUpperAccessory1None = false;
                    isUpperAccessory1Gun = false;
                    isUpperAccessory1Sword = true;     // Set true.
                    isUpperAccessory1Metallic = false;
                    isUpperAccessory1Backpack = false;

                    // Set Mix Output
                    soundSources[6].outputAudioMixerGroup = swordBus;



                    if (debugMode) { Debug.Log("Upper Body Accesory 1: Sword"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 1: Already set to Sword."); }
                }
                break;

            case UpperBodyAccesories.Gun:
                // If Upper Accessory 1 Gun Equals False.
                if (!isLowAccessory1Gun)
                {
                    // Set active clips.
                    ubEquipmentSounds1 = new List<AudioClip>(upperGun_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds1B.Clear();

                    // Set Bools
                    isUpperAccessory1None = false;
                    isUpperAccessory1Gun = true;    // Set true. 
                    isUpperAccessory1Sword = false;
                    isUpperAccessory1Metallic = false;
                    isUpperAccessory1Backpack = false;


                    // Set Mix Output
                    soundSources[6].outputAudioMixerGroup = gunBus;


                    if (debugMode) { Debug.Log("Upper Body Accesory 1: Small Gun"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 1: Already set to Gun."); }
                }
                break;


            case UpperBodyAccesories.Metallic:
                // If Low Accessory 1 Small Metallic Equals False.
                if (!isLowAccessory1Metallic)
                {
                    // Set active clips.
                    ubEquipmentSounds1 = new List<AudioClip>(upperMetallic_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds1B.Clear();

                    // Set Bools
                    isUpperAccessory1None = false;
                    isUpperAccessory1Gun = false;
                    isUpperAccessory1Sword = false;
                    isUpperAccessory1Metallic = true;    // Set true.
                    isUpperAccessory1Backpack = false;


                    // Set Mix Output
                    soundSources[6].outputAudioMixerGroup = metallicBus;

                    if (debugMode) { Debug.Log("Upper Body Accesory 1: Metallic"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 1: Already set to Metallic."); }
                }
                break;

            case UpperBodyAccesories.Backpack:
                // If Upper Accessory 1 Backpack Equals False.
                if (!isUpperAccessory1Backpack)
                {
                    // Set active clips.
                    ubEquipmentSounds1 = new List<AudioClip>(upperBackpack_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds1B.Clear();

                    // Set Bools
                    isUpperAccessory1None = false;
                    isUpperAccessory1Gun = false;
                    isUpperAccessory1Sword = false;
                    isUpperAccessory1Metallic = false;
                    isUpperAccessory1Backpack = true;   // Set true.


                    // Set Mix Output
                    soundSources[6].outputAudioMixerGroup = backpackBus;

                    if (debugMode) { Debug.Log("Upper Body Accesory 1: Backpack"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 1: Already set to Backpack."); }
                }
                break;
        }

        ///////////////////////////////////////////////////////////////////////////
        // ACCESSORY SOUNDSLOT 2
        switch (upperBodyAccesories2)
        {
            case UpperBodyAccesories.None:
                if (isUpperAccessory2None)
                {
                    // Clear active audio clips.
                    ubEquipmentSounds2.Clear();
                    ubEquipmentSounds2B.Clear();

                    if (debugMode)
                    {
                        Debug.Log("Upper Body Accesory 2: None");
                    }

                    // Set Bools
                    isUpperAccessory2None = true;     // Set true.
                    isUpperAccessory2Gun = false;
                    isUpperAccessory2Sword = false;
                    isUpperAccessory2Metallic = false;
                    isUpperAccessory2Backpack = false;

                }
                else
                {
                    { Debug.Log("Upper Body Acessory 2: Already set to none."); }
                }
                break;

            case UpperBodyAccesories.Sword:
                // If Upper Acessory 2 Sword Equals False
                if (!isUpperAccessory2Sword)
                {
                    // Set active clips.
                    ubEquipmentSounds2 = new List<AudioClip>(upperSword_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds2B.Clear();

                    // Set Bools
                    isUpperAccessory2None = false;
                    isUpperAccessory2Gun = false;
                    isUpperAccessory2Sword = true;     // Set true.
                    isUpperAccessory2Metallic = false;
                    isUpperAccessory2Backpack = false;



                    // Set Mix Output
                    soundSources[7].outputAudioMixerGroup = swordBus;

                    if (debugMode) { Debug.Log("Upper Body Accesory 2: Sword"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 2: Already set to Sword."); }
                }
                break;

            case UpperBodyAccesories.Gun:
                // If Upper Accessory 2 Gun Equals False.
                if (!isLowAccessory2Gun)
                {
                    // Set active clips.
                    ubEquipmentSounds2 = new List<AudioClip>(upperGun_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds2B.Clear();

                    // Set Bools
                    isUpperAccessory2None = false;
                    isUpperAccessory2Gun = true;    // Set true. 
                    isUpperAccessory2Sword = false;
                    isUpperAccessory2Metallic = false;
                    isUpperAccessory2Backpack = false;


                    // Set Mix Output
                    soundSources[7].outputAudioMixerGroup = gunBus;

                    if (debugMode) { Debug.Log("Upper Body Accesory 2: Small Gun"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 2: Already set to Gun."); }
                }
                break;


            case UpperBodyAccesories.Metallic:
                // If Upper Accessory 1 Small Metallic Equals False.
                if (!isLowAccessory2Metallic)
                {
                    // Set active clips.
                    ubEquipmentSounds2 = new List<AudioClip>(upperMetallic_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds2B.Clear();

                    // Set Bools
                    isUpperAccessory2None = false;
                    isUpperAccessory2Gun = false;
                    isUpperAccessory2Sword = false;
                    isUpperAccessory2Metallic = true;    // Set true.
                    isUpperAccessory2Backpack = false;


                    // Set Mix Output
                    soundSources[7].outputAudioMixerGroup = metallicBus;

                    if (debugMode) { Debug.Log("Upper Body Accesory 2: Metallic"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 2: Already set to Metallic."); }
                }
                break;

            case UpperBodyAccesories.Backpack:
                // If Upper Accessory 2 Backpack Equals False.
                if (!isUpperAccessory2Backpack)
                {
                    // Set active clips.
                    ubEquipmentSounds2 = new List<AudioClip>(upperBackpack_SGO.soundClips);
                    // Clear old clips.
                    ubEquipmentSounds2B.Clear();

                    // Set Bools
                    isUpperAccessory2None = false;
                    isUpperAccessory2Gun = false;
                    isUpperAccessory2Sword = false;
                    isUpperAccessory2Metallic = false;
                    isUpperAccessory2Backpack = true;   // Set true.


                    // Set Mix Output
                    soundSources[7].outputAudioMixerGroup = backpackBus;

                    if (debugMode) { Debug.Log("Upper Body Accesory 2: Backpack"); }
                }
                else
                {
                    { Debug.Log("Upper Body Acessory 2: Already set to Backpack."); }
                }
                break;

        }





    }

}


