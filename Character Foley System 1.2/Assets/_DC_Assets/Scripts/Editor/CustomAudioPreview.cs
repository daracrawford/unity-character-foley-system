﻿using System.Linq.Expressions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEditor;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

[CustomEditor(typeof(FootstepSystem))]  // Targets Sound Concatenation Script
public class CustomAudioPreview : Editor
{
    Color defBackgroundColor;

    
    public override void OnInspectorGUI()
    {

        // Script reference
        FootstepSystem footstepSystemScript = (FootstepSystem) target;

        

        // Debug mode
        EditorGUILayout.Space();
        footstepSystemScript.debugMode = EditorGUILayout.Toggle("Debug Mode", footstepSystemScript.debugMode);
        EditorGUILayout.Space();

        // Audio Preview & Reset
        EditorGUILayout.LabelField("Preview, Reset, or Apply Active Inspector Settings", (EditorStyles.boldLabel));
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Audio Preview", GUILayout.Width(100)))
        {
            //Debug.Log("Audio preview");
            footstepSystemScript.Footstep();
        }
        if (GUILayout.Button("Land Preview", GUILayout.Width(100)))
        {
            //Debug.Log("Audio preview");
            footstepSystemScript.CharacterLand();
        }

        if (GUILayout.Button("Reset", GUILayout.Width(100)))
        {
            //Debug.Log("Audio reset");
            footstepSystemScript.Reset();
        }
        if (GUILayout.Button("Apply Settings", GUILayout.Width(100)))
        {
            //Debug.Log("Apply Settings");
            footstepSystemScript.ApplySettings();
        }
        GUILayout.EndHorizontal();
        EditorGUILayout.Space();

        // Surface Switches
        EditorGUILayout.LabelField("Switch Surface", (EditorStyles.boldLabel));
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Concrete", GUILayout.Width(100)))
        {
            Debug.Log("Concrete Switch");
            footstepSystemScript.ConcreteSteps();
        }
        
        if (GUILayout.Button("Wood", GUILayout.Width(100)))
        {
            Debug.Log("Wood Switch");
            footstepSystemScript.WoodSteps();
        }
        if (GUILayout.Button("Sand Shallow", GUILayout.Width(100)))
        {
            Debug.Log("Sand Switch");
            footstepSystemScript.SandShallowSteps();
        }
        if (GUILayout.Button("Dirt", GUILayout.Width(100)))
        {
            Debug.Log("Dirt Switch");
            footstepSystemScript.DirtSteps();
        }

        GUILayout.EndHorizontal();
        
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Grass", GUILayout.Width(100)))
        {
            Debug.Log("Grass Switch");
            footstepSystemScript.GrassSteps();
        }
        if (GUILayout.Button("Gravel Bright", GUILayout.Width(100)))
        {
            Debug.Log("Gravel Switch");
            footstepSystemScript.GravelBrightSteps();
        }
        if (GUILayout.Button("Tile", GUILayout.Width(100)))
        {
            Debug.Log("Tile Bright Switch");
            footstepSystemScript.TileBrightSteps();
        }
        if (GUILayout.Button("Ice", GUILayout.Width(100)))
        {
            Debug.Log("Ice Switch");
            footstepSystemScript.IceSteps();
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Mud", GUILayout.Width(100)))
        {
            Debug.Log("Mud Switch");
            footstepSystemScript.MudSteps();
        }
        if (GUILayout.Button("Snow Shallow", GUILayout.Width(100)))
        {
            Debug.Log("Snow Shallow Switch");
            footstepSystemScript.SnowShallowSteps();
        }
        if (GUILayout.Button("Water", GUILayout.Width(100)))
        {
            Debug.Log("Water Switch");
            footstepSystemScript.WaterSteps();
        }
        if (GUILayout.Button("Carpet", GUILayout.Width(100)))
        {
            Debug.Log("Carpet Switch");
            footstepSystemScript.CarpetSteps();
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Sand Deep", GUILayout.Width(100)))
        {
            Debug.Log("Sand Deep Switch");
            footstepSystemScript.SandDeepSteps();
        }

        if (GUILayout.Button("Sand Wet", GUILayout.Width(100)))
        {
            Debug.Log("Sand Wet Switch");
            footstepSystemScript.SandWetSteps();
        }
        if (GUILayout.Button("Gravel Dark", GUILayout.Width(100)))
        {
            Debug.Log("Gravel Dark Switch");
            footstepSystemScript.GravelDarkSteps();
        }
        if (GUILayout.Button("Snow Deep", GUILayout.Width(100)))
        {
            Debug.Log("Snow Deep Switch");
            footstepSystemScript.SnowDeepSteps();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Tile Dark", GUILayout.Width(100)))
        {
            Debug.Log("Tile Dark Switch");
            footstepSystemScript.TileDarkSteps();}


        if (GUILayout.Button("Metal Soft", GUILayout.Width(100)))
        {
            Debug.Log("Metal Soft Switch");
            footstepSystemScript.MetalSoftSteps();
        }

        if (GUILayout.Button("Metal Bright", GUILayout.Width(100)))
        {
            Debug.Log("Metal Bright Switch");
            footstepSystemScript.MetalBrightSteps();
        }

        if (GUILayout.Button("Metal Dark", GUILayout.Width(100)))
        {
            Debug.Log("Metal Dark Switch");
            footstepSystemScript.MetalDarkSteps();
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Custom 1", GUILayout.Width(100)))
        {
            Debug.Log("Custom 1 Switch");
            footstepSystemScript.Custom1Steps();
        }


        if (GUILayout.Button("Custom 2", GUILayout.Width(100)))
        {
            Debug.Log("Custom 2 Switch");
            footstepSystemScript.Custom2Steps();
        }

        if (GUILayout.Button("Custom 3", GUILayout.Width(100)))
        {
            Debug.Log("Custom 3");
            footstepSystemScript.Custom3Steps();
        }

        if (GUILayout.Button("Custom 4", GUILayout.Width(100)))
        {
            Debug.Log("Custom 4 Switch");
            footstepSystemScript.Custom4Steps();
        }

        GUILayout.EndHorizontal();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();


        // HEEL TOE
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Toe Delay Settings", (EditorStyles.boldLabel));
        GUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        footstepSystemScript.heelToeAutomation = EditorGUILayout.Toggle("Heel Toe Automation", footstepSystemScript.heelToeAutomation);

        if (footstepSystemScript.heelToeAutomation)
        {
            GUILayout.Label("Response Time");
            footstepSystemScript.heelToeIntervalTime =
                EditorGUILayout.Slider(footstepSystemScript.heelToeIntervalTime, 0.1f, 1f);
            Debug.Log("Heel Toe Automation Enabled");
        }
        else
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Toe Clip Delay");
            footstepSystemScript.toeDelay = EditorGUILayout.Slider((float)footstepSystemScript.toeDelay, 0f, 1f);
            GUILayout.EndHorizontal();
        }
        GUILayout.EndHorizontal();
      




       

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Quick Presets", (EditorStyles.boldLabel));

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Clean", GUILayout.Width(100)))
        {
            Debug.Log("Preset Clean");
            footstepSystemScript.PresetClean();
        }
        if (GUILayout.Button("Subtle", GUILayout.Width(100)))
        {
            Debug.Log("Preset Subtle Modulation");
            footstepSystemScript.PresetSubtleModulation();
        }
        if (GUILayout.Button("Moderate", GUILayout.Width(100)))
        {
            Debug.Log("Preset Moderate Modulation");
            footstepSystemScript.PresetModerateModulation();
        }
        if (GUILayout.Button("Heavy", GUILayout.Width(100)))
        {
            Debug.Log("Preset Moderate Modulation");
            footstepSystemScript.PresetHeavyModulation();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        // Volume management. 
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Volume Management", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Set Base Volume");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        footstepSystemScript.volume = EditorGUILayout.Slider(footstepSystemScript.volume, 0f, 1f);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Amount To Randomly Add Or Subtract From The Base Volume");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        footstepSystemScript.randomVolume = EditorGUILayout.Slider(footstepSystemScript.randomVolume, 0f, 0.3f);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();


        // Pitch management.
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Pitch Management", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();

        footstepSystemScript.pitchEnable = EditorGUILayout.Toggle("Enable Pitch Modulation", footstepSystemScript.pitchEnable);
        EditorGUILayout.Space();

        // If pitch is enabled, then show pitch controls
        if (footstepSystemScript.pitchEnable)
        {
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Set Base Pitch. < Heavy character.  > Light character");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.pitch = EditorGUILayout.Slider(footstepSystemScript.pitch, 0.5f, 1.5f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Amount To Randomly Add Or Subtract From The Base Pitch");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.randomPitch = EditorGUILayout.Slider(footstepSystemScript.randomPitch, 0f, 0.3f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }
        EditorGUILayout.Space();
        EditorGUILayout.Space();


        // Low Pass Filter management.
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Low Pass Filter Management", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();

        footstepSystemScript.lpf_Enable = EditorGUILayout.Toggle("Enable LPF Modulation", footstepSystemScript.lpf_Enable);
        EditorGUILayout.Space();

        // If LPF enabled, show controls.
        if (footstepSystemScript.lpf_Enable)
        {
            EditorGUILayout.Space();


            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Frequency Cut Off");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.lpf_FreqCut = EditorGUILayout.Slider(footstepSystemScript.lpf_FreqCut, 10, 22000);
            serializedObject.ApplyModifiedProperties();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Amount To Randomly Add Or Cut From The Base Frequency");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.lpf_Random = EditorGUILayout.Slider(footstepSystemScript.lpf_Random, 0, 10000f);
            EditorGUILayout.EndHorizontal();

            // Resonance
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Resonate Filtered Q");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.lpf_Resonance = EditorGUILayout.Slider(footstepSystemScript.lpf_Resonance, footstepSystemScript.lpf_ResMin, footstepSystemScript.lpf_ResMax);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Amount To Randomly Modulate");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.lpf_ResRandom = EditorGUILayout.Slider(footstepSystemScript.lpf_ResRandom, 0f, 2f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        // High Pass Filter management.
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("High Pass Filter Management", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();
        
        footstepSystemScript.hpf_Enable = EditorGUILayout.Toggle("Enable HPF Modulation", footstepSystemScript.hpf_Enable);
        EditorGUILayout.Space();

        if (footstepSystemScript.hpf_Enable)
        {
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Frequency Cut Off");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.hpf_FreqCut = EditorGUILayout.Slider(footstepSystemScript.hpf_FreqCut, 0, 22000);
            serializedObject.ApplyModifiedProperties();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Amount To Randomly Add Or Cut From The Base Frequency");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.hpf_Random = EditorGUILayout.Slider(footstepSystemScript.hpf_Random, 0, 10000f);
            EditorGUILayout.EndHorizontal();

            // Resonance
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Resonate Filtered Q");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.hpf_Resonance = EditorGUILayout.Slider(footstepSystemScript.hpf_Resonance, footstepSystemScript.hpf_ResMin, footstepSystemScript.hpf_ResMax);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Amount To Randomly Modulate");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.hpf_ResRandom = EditorGUILayout.Slider(footstepSystemScript.hpf_ResRandom, 0f, 2f);
            EditorGUILayout.EndHorizontal();


            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();


        // Footstep nuisance manager.
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Footstep Nuisance Controller", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();

        footstepSystemScript.footstepNuisanceControl = EditorGUILayout.Toggle("Enable Nuisance Control", footstepSystemScript.footstepNuisanceControl);
        EditorGUILayout.Space();

        if (footstepSystemScript.footstepNuisanceControl)
        {
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Set Taget Volume.  < Target Volume. > Default volume.");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.stepReductionTarget = EditorGUILayout.Slider(footstepSystemScript.stepReductionTarget, 0f, 1f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Speed to target. < Slowest. > Quickest");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            footstepSystemScript.targetSpeed = EditorGUILayout.Slider(footstepSystemScript.targetSpeed, 0.001f, 0.2f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

        }
        

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        // Saves user settings when tweaking the inspector.
        EditorUtility.SetDirty(footstepSystemScript);


        // Draws default inspector
        DrawDefaultInspector();




    }
   

}
