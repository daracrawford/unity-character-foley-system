﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// Targets Character Equipped Script
[CustomEditor(typeof(CharacterEquipped))]
public class CustomEquipPreview : Editor
{
    Color defBackgroundColor;

    // Script reference.
    public CharacterEquipped characterEquipped;

    public override void OnInspectorGUI()
    {
        // Script reference, needs casted to work.
        characterEquipped = (CharacterEquipped) target;


        // Debug mode
        EditorGUILayout.Space();
        characterEquipped.debugMode = EditorGUILayout.Toggle("Debug Mode", characterEquipped.debugMode);
        EditorGUILayout.Space();

        // Audio Preview & Reset
        EditorGUILayout.LabelField("Preview or Reset Audio Pool", (EditorStyles.boldLabel));
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Audio Preview", GUILayout.Width(150)))
        {
            //Debug.Log("Audio preview");
            characterEquipped.CharacterMovement();
        }
        if (GUILayout.Button("Reset", GUILayout.Width(150)))
        {
            //Debug.Log("Audio reset");
            characterEquipped.ResetAudio();
        }
        GUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Character Clothing", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();

        GUILayout.Label("Lower Body");

        // Clothing lower enum 1
        characterEquipped.lowerBodyClothing1 =
            (CharacterEquipped.LowerBodyClothing) EditorGUILayout.EnumPopup("Material 1",
                characterEquipped.lowerBodyClothing1); 
        // Clothing lower enum 2
        characterEquipped.lowerBodyClothing2 =
            (CharacterEquipped.LowerBodyClothing)EditorGUILayout.EnumPopup("Material 2",
                characterEquipped.lowerBodyClothing2);


        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Upper Body");
        EditorGUILayout.EndHorizontal();


        // Clothing upper enum 1
        characterEquipped.upperBodyClothing1 =
            (CharacterEquipped.UpperBodyClothing)EditorGUILayout.EnumPopup("Material 1",
                characterEquipped.upperBodyClothing1);
        // Clothing upper enum 2
        characterEquipped.upperBodyClothing2 =
            (CharacterEquipped.UpperBodyClothing)EditorGUILayout.EnumPopup("Material 2",
                characterEquipped.upperBodyClothing2);

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Character Accessories", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();
        GUILayout.Label("Lower Body");


        // Acessories lower enum 1
        characterEquipped.lowerBodyAccesories1 =
            (CharacterEquipped.LowerBodyAccesories)EditorGUILayout.EnumPopup("Accessory 1",
                characterEquipped.lowerBodyAccesories1);
        // Accessories lower enum 2
        characterEquipped.lowerBodyAccesories2 =
            (CharacterEquipped.LowerBodyAccesories)EditorGUILayout.EnumPopup("Accessory 2",
                characterEquipped.lowerBodyAccesories2);


        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Upper Body");
        EditorGUILayout.EndHorizontal();


        // Acessories upper enum 1
        characterEquipped.upperBodyAccesories1 =
            (CharacterEquipped.UpperBodyAccesories)EditorGUILayout.EnumPopup("Accessory 1",
                characterEquipped.upperBodyAccesories1);
        // Accessories upper enum 2
        characterEquipped.upperBodyAccesories2 =
            (CharacterEquipped.UpperBodyAccesories)EditorGUILayout.EnumPopup("Accessory 2",
                characterEquipped.upperBodyAccesories2);


        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        // Volume management. 
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Volume Management", (EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Set Base Volume");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        characterEquipped.volume = EditorGUILayout.Slider(characterEquipped.volume, 0f, 1f);
        EditorGUILayout.EndHorizontal();

      
        characterEquipped.volumeMod = EditorGUILayout.Toggle("Enable Volume Modulation", characterEquipped.volumeMod);
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();


        // If volume is enabled, then show volume controls
        if (characterEquipped.volumeMod)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Amount To Randomly Add Or Subtract From The Base Volume");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            characterEquipped.randomVolume = EditorGUILayout.Slider(characterEquipped.randomVolume, 0f, 0.3f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        // Pitch management. 
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Set Base Pitch. < Heavy character.  > Light character");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        characterEquipped.pitch = EditorGUILayout.Slider(characterEquipped.pitch, 0.5f, 1.5f);
        EditorGUILayout.EndHorizontal();

        characterEquipped.pitchMod = EditorGUILayout.Toggle("Enable Pitch Modulation", characterEquipped.pitchMod);

        if (characterEquipped.pitchMod)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Amount To Randomly Add Or Subtract From The Base Pitch");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            characterEquipped.randomPitch = EditorGUILayout.Slider(characterEquipped.randomPitch, 0f, 0.3f);
            EditorGUILayout.EndHorizontal();
        }

        

        

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();




        // Saves inspector values.
        EditorUtility.SetDirty(characterEquipped);


        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();


        DrawDefaultInspector();
    }

    

}
