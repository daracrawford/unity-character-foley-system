﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
[CreateAssetMenu(menuName = "Add New Clothing Type", fileName = "Clothing Material", order = 1)]
public class CharacterClothing : ScriptableObject
{
    [Header("Add Audio Clips")]
    [SerializeField] public List<AudioClip> soundClips;

}
