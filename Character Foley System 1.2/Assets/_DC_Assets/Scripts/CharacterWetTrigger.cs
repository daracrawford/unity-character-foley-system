﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CharacterWetTrigger : MonoBehaviour
{
    // Script Reference
    public FoleyEventTrigger foleyTriggerScript;
    public FootstepSystem footstepScript;

    public bool debugMode;
    
    
    // Bus Reference
    public AudioMixerGroup wetfeet;
    public AudioMixerGroup wetClothes;


    // Mixer
    private float mixerVolume;
    private float mixerSaveVolme;

   
    // Gradually reduces volume.
    [Range(0, 10)] public float characterDrySpeed;

    // Checks if character is in water.   
    private bool inWater = false;

    void Start()
    {
        // Gets mixer volume setting on start.
        wetfeet.audioMixer.GetFloat("volWetFeet", out mixerSaveVolme);
    }

    

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            // In water equals false.
            inWater = false;

            // Call Character wet method.
            StartCoroutine(CharacterWet());
            //Debug.Log("Character out of water, character wet method triggered");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // If colliding with player, then the player is in water.
        if (other.gameObject.CompareTag("Player"))
        {
            inWater = true;
            //Debug.Log("Player is in water");
        }
    }

    

    IEnumerator CharacterWet()
    {
        // Is character wet equals true.
        footstepScript.isCharacterWet = true;

        // Reset Volume.
        wetfeet.audioMixer.SetFloat("volWetFeet", mixerSaveVolme);

        // Reset Mixer Variable.
        mixerVolume = mixerSaveVolme;


        while (mixerVolume > -80)
        {
            // Current Mixer Volume.
            wetfeet.audioMixer.GetFloat("volWetFeet", out mixerVolume);

            // New Volume subtract deltatime.
            mixerVolume -= Time.deltaTime * characterDrySpeed;
            
            // Volume Mixer Equals = Mixer Volume.
            wetfeet.audioMixer.SetFloat("volWetFeet", mixerVolume);

            // Debug
            print(wetfeet.audioMixer.GetFloat("volWetFeet", out mixerVolume));
             
            // If character is in water, break current while loop.
            if (inWater)
            {
                break;
            }

            yield return null;
        }

        footstepScript.isCharacterWet = false;
        Debug.Log("Character Is Now Dry");
    }


}
